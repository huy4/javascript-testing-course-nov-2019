# JavaScript Mocking Fundamentals

## Tổng quan
1. Run ESLint with Jest using jest-runner-eslint
2. Run only relevant Jest tests on git commit to avoid breakages

## Chi tiết
1. Run ESLint with Jest using jest-runner-eslint
	- Cách cài đặt
	```
	npm install --save-dev jest-runner-eslint
	```
	```
	jest.lint.js
	
	const path = require('path')

	module.exports = {
	  rootDir: path.join(__dirname, '..'),
	  displayName: 'lint',
	  runner: 'jest-runner-eslint',
	  testMatch: ['<rootDir>/**/*.js'],
	}
	```
	
	```
	package.json
	
	"scripts": {
	 .
	 .
	 .
	  "lint": "jest --config test/jest.lint.js",
	  "setup": "npm install && npm run validate"
	},
	"jest-runner-eslint": {
	  "clipOptions": {
		"ignorePath": "./.gitignore
	  }
	}
	```
	
	```
	jest.config.js
	
	projects: [
		'./test/jest.lint.js',
		'./test/jest.client.js',
		'./test/jest.server.js',
	],
	```
	
	- Cách chạy:
	
	```
	npm test
	```
	
2. Run only relevant Jest tests on git commit to avoid breakages
	- Sử dụng husky và lint-staged để kiểm tra những lỗi thuộc về test trong quá trình commit
	- Cách cài đặt
	```
	package.json
	
	"husky": {
		"hooks": {
		  "pre-commit": "lint-staged && npm run build"
		}
	  },
	  "lint-staged": {
		"**/*.+(js|json|css|html|md)": [
		  "prettier",
		  "jest --findRelatedTests",
		  "git add"
		]
	},
	```
	
## Đính kèm