# Static Analysis Testing JavaScript Applications


## Tổng quan

1. Lint JavaScript by configuring and running ESLint
2. Format Code by installing and running Prettier
3. Configure Prettier
4. Disable Unnecessary ESLint Stylistic Rules with eslint-config-prettier
5. Validate all files are formatted when linting
6. Avoid Common Errors with Flow Type Definitions
7. Validate Code in a pre-commit git Hook with husky
8. Auto-format all files and validate relevant files in a precommit script with lint-staged

## Chi tiết

1. Lint JavaScript by configuring and running ESLint
	- Install ESLint
		```
		in terminal
		
		npm install --save-dev eslint
		```
		
	- Run ESLint
		```
		npx lint src
		(src: thư mục chứa các file lint chạy qua)
		```
		
	- Configure ESLint
		- Tạo file `.eslintrc` để cấu hình cho ESLint
		- Một số thuộc tính cơ bản như:
			- extends: kế thừa các rule có sẵn của đơn vị khác
			- parserOptions: {
				ecmaVersion: 2018
			}
			- rules: cấu hình các luật
			
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-2)
	
2. Format Code by installing and running Prettier
	- Prettier là tool dùng để giảm thời gian formatting code
	- Install Prettier
		```
		npm install --save-dev prettier
		```

	- Run Prettier
		```
		npm run format
		
		packages.json
		
		"scripts": {
			"lint": "eslint src",
			"format": "prettier --write \"**/*.+(js|jsx|json|yml|yaml|css|less|scss|ts|tsx|md|mdx|graphql|vue)\""
		},
		```
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-3)

3. Configure Prettier
	- Tạo file `.pretierrc` để cấu hình Prettier, cài các rule trực quen trên https://prettier.io/playground/
	- Tạo file `.prettierignore` để Prettier không chạy qua
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-4)

4. Disable Unnecessary ESLint Stylistic Rules with eslint-config-prettier
	- Vì Prettier có thể tự động fix các lỗi trong codebase, nên việc eslint kiểm tra lỗi là không cần nếu như lỗi đó đã được Prettier fix
	- Để hỗ trợ việc này chúng ta có thể sử dụng tool `eslint-config-prettier`
	- `eslint-config-prettier` bỏ những rule không cần thiết hoặc có conflict với Prettier
	- Install `eslint-config-prettier `
		```
		npm install --save-dev eslint-config-prettier
		```
	
	- Configure `eslint-config-prettier`
		```
		.eslint
		
		"extends": [
			"eslint:recommended", "eslint-config-prettier"
		],
		```
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-5)
5. Validate all files are formatted when linting
	- Sử dụng `--list-different` để validate các file không theo định dạng của Prettier
	- Configure
	```
	"scripts": {
		"lint": "eslint src",
		"format": "npm run prettier -- --write",
		"prettier": "prettier \"**/*.+(js|jsx|json|yml|yaml|css|less|scss|ts|tsx|md|graphql|mdx)\"",
		"validate": "npm run lint && npm run prettier -- --list-different"
	},
	```
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-6)
	
6. Avoid Common Errors with Flow Type Definitions
	- Sử dụng `Flow` để tránh những lỗi về kiểu dữ liệu
	- Install Flow
		`npm run --save-dev flow-bin`
	- Configure Flow
		```
		packages.json
		
		"scripts": {
			"lint": "eslint src",
			"flow": "flow",
			"format": "npm run prettier -- --write",
			"prettier": "prettier \"**/*.+(js|jsx|json|yml|yaml|css|less|scss|ts|tsx|md|graphql|mdx)\"",
			"validate": "npm run lint && npm run prettier -- --list-different && npm run flow"
		},
		```
		
		`npm run flow init`
		
	- Run Flow:
		`npm run flow` or `npm run validate`
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-7)

7. Validate Code in a pre-commit git Hook with husky
	- Sử dụng `Husky` để thực hiện những công việc kiểm tra như lint trước khi commit
	- Điều này giúp nhanh chóng tìm ra lỗi nếu có, không cần chờ đến lúc CI báo lỗi.
	
	Configure Husky old version:
	```
	"scripts": {
		"lint": "eslint src",
		"format": "npm run prettier -- --write",
		"prettier": "prettier \"**/*.+(js|jsx|json|yml|yaml|css|less|scss|ts|tsx|md|graphql|mdx)\"",
		"validate": "npm run lint && npm run prettier -- --list-different && npm run flow",
		"precommit": "npm run validate"
	},
	```
	
	Configure Husky new version:
	
	```
	{
	  "husky": {
		"hooks": {
		  "pre-commit": "npm run validate"
		}
	  }
	}
	```
	Run Husky
	- Tự động chạy khi chạy lệnh git commit
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-8)

8. Auto-format all files and validate relevant files in a precommit script with lint-staged
	- Validate bằng ESLint tốn nhiều thời gian vì phải duyệt toàn bộ file
	- Giải pháp đưa ra là lint-staged
	- `lint-staged` quét lint ở những file có thay đổi
	- Configure `lint-staged` bằng 2 cách:
	
	```
	package.json

	{
	  "lint-staged": {
		"*": "your-cmd"
	  }
	}
	
	.lintstagedrc
	
	{
	  "*": "your-cmd"
	}
	```
	
	Ví dụ:
	```
	{
	  "linters": {
		"*.js": [
		  "eslint"
		],
		"**/*.+(js|jsx|json|yml|yaml|css|less|scss|ts|tsx|md|graphql|mdx)": [
		  "prettier --write",
		  "git add"
		]
	  }
	}
	```
	
	- Chạy lint-staged qua precommit
	- [Link github](https://github.com/kentcdodds/static-testing-tools/tree/step-9)

## Chi tiết
