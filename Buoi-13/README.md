# Test React Components with Jest and React Testing Library

## Tổng quan
1. Test componentDidCatch handler error boundaries with react-testing-library
2. Hide console.error Logs when Testing Error Boundaries with jest.spyOn

## Chi tiết
1. Test componentDidCatch handler error boundaries with react-testing-library
	- componentDidCatch sẽ bắt lỗi nếu các children component xảy ra lỗi, đồng thời rerender thông báo lỗi
	- Mục tiêu test
		- Rerender được Error và test các case về tham số truyền vào, số lần gọi hàm
	- Test như thế nào
		- Tạo một children component return Error
		```
		function Bomb({shouldThrow}) {
		  if (shouldThrow) {
			throw new Error('💣')
		  } else {
			return null
		  }
		}
		```
		- Rerender component để xảy ra lỗi
		```
		test('calls report error and renders that there was a problem', () => {
		  const {rerender} = render(<ErrorBoundary><Bomb /></ErrorBoundary>)

		  rerender(<ErrorBoundary><Bomb shouldThrow={true} /></ErrorBoundary>)
		})
		```
		- Source code chứa HTTP request nên mock HTTP request
		```
		import {reportError as mockReportError} from '../api'

		jest.mock('../api')
		```
		- Test HTTP request có được gọi bằng đầu vào là những tham số (error, info)
		```
		const error = expect.any(error)
		const info = {componentStack: expect.stringContaining('Bomb')}
		expect(mockReportError).toHaveBeenCalledWith(error, info)
		```
		- Sau khi test xong nên xóa mock để đảm bảo cho các test khác không chịu ảnh hưởng
		```
		afterEach(() => {
		  jest.clearAllMocks()
		})
		```
		- Source test hoàn chỉnh đính kèm ở dưới

2. Hide console.error Logs when Testing Error Boundaries with jest.spyOn
	- Problem:
		- Khi test Error Boundaries component, chỉ ra lỗi là Error Boundaries component đã test được nhưng lỗi sẽ hiểu thị ở console log
	- Solutions:
		- Sử dụng jest.spyOn để mock console.error
		```
		beforeAll(() => {
		  jest.spyOn(console, 'error').mockImplementation(() => {})
		})
		```

## Đính kèm
	- [Github source test hoàn chỉnh](https://github.com/kentcdodds/react-testing-library-course/blob/tjs/src/__tests__/error-boundary-01.js)