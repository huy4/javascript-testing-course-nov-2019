# Install, Configure, and Script Cypress for JavaScript Web Applications

## Tổng quan
1. Use cy.request from Cypress to Authenticate as a New User
2. Use a Custom Cypress Command to Login as a User
3. Combine Custom Cypress Commands into a Single Custom Command
4. Install React DevTools with Cypress

## Chi tiết
1. Use cy.request from Cypress to Authenticate as a New User
	- cy.request set token đã authenticate
	```
	cy.request({
	  url: 'http://localhost:3000/login',
	  method: 'POST',
	  body: user,
	}).then(response => {
	  window.localStorage.setItem('token', response.body.user.token)
	})
	```
	- Test E2E
	```
	calculator.js
	
	describe('authenticated calculator', () => {
	  it('displays the username', () => {
		cy.createUser().then(user => {
		  cy.visit('/')
			.findByTestId('username-display')
			.should('have.text', user.username)
			.findByText(/logout/i)
			.click()
			.findByTestId('username-display')
			.should('not.exist')
		})
	  })
	})
	```

2. Use a Custom Cypress Command to Login as a User
	- Thêm phương thức login
	```
	commands.js
	
	Cypress.Commands.add('login', user => {
		cy.request({
			url: 'http://localhost:3000/login',
			method: 'POST',
			body: user,
		  })
		  .then(response => {
			window.localStorage.setItem('token', response.body.user.token)
		  })
		})
	```
	- Test E2E
	```
	calculator.js
	
	describe('authenticated calculator', () => {
	  it('displays the username', () => {
		cy.createUser().then(user => {
		  cy.login(user)
			.visit('/')
			.findByTestId('username-display')
			.should('have.text', user.username)
			.findByText(/logout/i)
			.click()
			.findByTestId('username-display')
			.should('not.exist')
		})
	  })
	})
	```

3. Combine Custom Cypress Commands into a Single Custom Command
	- Test case authenticated calculator bao gồm createUser và login có thể gộp 2 bước này lại bằng Cypress Commands
	```
	commands.js
	
	Cypress.Commands.add('loginAsNewUser', () => {
	  cy.createUser().then(user => {
		cy.login(user)
	  })
	})
	```
	- Login return user
	```
	commands.js
	
	Cypress.Commands.add('login', user => {
	  return cy
		.request({
		  url: 'http://localhost:3000/login',
		  method: 'POST',
		  body: user,
		})
		.then(response => {
		  window.localStorage.setItem('token', response.body.user.token)
		  return {...response.body.user, ...user}
		})
	})
	```
	- Sử dụng phương thức `loginAsNewUser`
	```
	describe('authenticated calculator', () => {
	  it('displays the username', () => {
		cy.loginAsNewUser().then(user => {
		  cy.visit('/')
			.findByTestId('username-display')
			.should('have.text', user.username)
			.findByText(/logout/i)
			.click()
			.findByTestId('username-display')
			.should('not.exist')
		})
	  })
	})
	```
4. Install React DevTools with Cypress
	- Cài đặt [React DevTools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) vào Cypress
	- Để app nhận được React DevTools thêm đoạn script sau vào thẻ head hoặc thêm vào trước tất cả các đoạn script
	```
	<script>
		if(window.Cypress) {
		  window.__REACT_DEVTOOLS_GLOBAL_HOOK__ =
		}
	</script>
	```