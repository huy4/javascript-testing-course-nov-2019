# Install, Configure, and Script Cypress for JavaScript Web Applications

## Tổng quan
1. Simulate HTTP Errors in Cypress Tests
2. Test User Login with Cypress
3. Create a User with cy.request from Cypress
4. Keep Tests Isolated and Focused with Custom Cypress Commands
5. Use Custom Cypress Command for Reusable Assertions
6. Run Tests as an Authenticated User with Cypress

## Chi tiết
1. Simulate HTTP Errors in Cypress Tests
	- Mock server response errors
	```
	register.js
	
	it(`should show an error message if there's an error registering`, () => {
	  cy.server()
	  cy.route({
		method: 'POST',
		url: 'http://localhost:3000/register',
		status: 500,
		response: {},
	  })
	  cy.visit('/register')
		.findByText(/submit/i)
		.click()
		.findByText(/error.*try again/i)
	})
	```
	- Lấy url response server ở tab network của Chrome
	![](https://res.cloudinary.com/dg3gyk0gu/image/upload/v1574727281/transcript-images/cypress-simulate-http-errors-in-cypress-tests-passing-tests-with-error.png)
	
2. Test User Login with Cypress
	- Kịch bản test User Login bằng Cypress
		- Đăng ký tài khoản
		- Logout
		- Đăng nhập bằng tài khoản vừa đăng ký
		- Kiểm tra login thành giống register thành công
		```
		login.js
		
		describe('login', () => {
		  it('should login an existing user', () => {
			const user = buildUser()
			cy.visit('/')
			  .findByText(/register/i)
			  .click()
			  .findByLabelText(/username/i)
			  .type(user.username)
			  .findByLabelText(/password/i)
			  .type(user.password)
			  .findByText(/submit/i)
			  .click()
			  .findByText(/logout/i)
			  .click()
			  // now our test can start...
			  .findByText(/login/i)
			  .click()
			  .findByLabelText(/username/i)
			  .type(user.username)
			  .findByLabelText(/password/i)
			  .type(user.password)
			  .findByText(/submit/i)
			  .click()
			  // now let's verify things are set after login.
			  .url()
			  .should('eq', `${Cypress.config().baseUrl}/`)
			  .window()
			  .its('localStorage.token')
			  .should('be.a', 'string')
			  .findByTestId('username-display')
			  .should('have.text', user.username)
		  })
		})
		```

3. Create a User with cy.request from Cypress
	- Mock User request bằng cy.request
		- Mở tab network của chrome để xem url, body
		- ![](https://res.cloudinary.com/dg3gyk0gu/image/upload/v1574727281/transcript-images/cypress-create-a-user-with-cy-request-from-cypress-request-body.png)
		- Thay thế bước register bằng cy.request
		```
		login.js
		
		describe('login', () => {
		  it('should login an existing user', () => {
			// create user
			const user = buildUser()
			cy.request({
			  url: 'http://localhost:3000/register',
			  method: 'POST',
			  body: user,
			})

			cy.visit('/')
			  .findByText(/login/i)
			  .click()
			  .findByLabelText(/username/i)
			  .type(user.username)
			  .findByLabelText(/password/i)
			  .type(user.password)
			  .findByText(/submit/i)
			  .click()
			  // now let's verify things are set after login.
			  .url()
			  .should('eq', `${Cypress.config().baseUrl}/`)
			  .window()
			  .its('localStorage.token')
			  .should('be.a', 'string')
			  .findByTestId('username-display')
			  .should('have.text', user.username)
		  })
		})
		```
		
4. Keep Tests Isolated and Focused with Custom Cypress Commands
	- `Cypress.Commands.add` để thêm phương thức createUser
	```
	command.js
	
	import {buildUser} from '../support/generate'

	Cypress.Commands.add('createUser', overrides => {
	  const user = buildUser(overrides)
	  cy.request({
		url: 'http://localhost:3000/register',
		method: 'POST',
		body: user,
	  }).then(response => ({...response.body.user, ...user}))
	})
	```
	- Sử dụng createUser ở test e2e
	```
	describe('login', () => {
	  it('should login an existing user', () => {
		cy.createUser().then(user => {
		  cy.visit('/')
			.findByText(/login/i)
			.click()
			.findByLabelText(/username/i)
			.type(user.username)
			.findByLabelText(/password/i)
			.type(user.password)
			.findByText(/submit/i)
			.click()
			// now let's verify things are set after login.
			.url()
			.should('eq', `${Cypress.config().baseUrl}/`)
			.window()
			.its('localStorage.token')
			.should('be.a', 'string')
			.findByTestId('username-display')
			.should('have.text', user.username)
		})
	  })
	})
	```

5. Use Custom Cypress Command for Reusable Assertions
	- Test E2E login và register có cùng assertions nên dùng Cypress Command để thêm các phương thức cho assertions
		- Assertion url nhận về là baseUrl
		```
		commands.js
		
		Cypress.Commands.add('assertHome', () => {
		  cy.url().should('eq', `${Cypress.config().baseUrl}/`)
		})
		```
		- Assertion username và localStorage
		```
		commands.js
		
		Cypress.Commands.add('assertLoggedInAs', user => {
		  cy.window()
			.its('localStorage.token')
			.should('be.a', 'string')
			.findByTestId('username-display')
			.should('have.text', user.username)
		})
		```
	- Sử dụng Assertions vừa thêm
	```
	register.js
	
	describe('registration', () => {
	  it('should register a new user', () => {
		const user = buildUser()
		cy.visit('/')
		  .findByText(/register/i)
		  .click()
		  .findByLabelText(/username/i)
		  .type(user.username)
		  .findByLabelText(/password/i)
		  .type(user.password)
		  .findByText(/submit/i)
		  .click()
		  .assertHome()
		  .assertLoggedInAs(user)
	  })
	  ...
	})
	```
	```
	login.js
	
	describe('login', () => {
	  it('should login an existing user', () => {
		cy.createUser().then(user => {
		  cy.visit('/')
			.findByText(/login/i)
			.click()
			.findByLabelText(/username/i)
			.type(user.username)
			.findByLabelText(/password/i)
			.type(user.password)
			.findByText(/submit/i)
			.click()
			.assertHome()
			.assertLoggedInAs(user)
		})
	  })
	})
	```

6. Run Tests as an Authenticated User with Cypress
	- Test với trường hợp user đã được xác thực hay chưa
	- Nếu user đã xác thực thì sẽ có tên hiển thị còn ngược lại thì không
	```
	describe('authenticated calculator', () => {
	  it('displays the username', () => {
		cy.createUser().then(user => {
		  cy.visit('/')
			.findByText(/login/i)
			.click()
			.findByLabelText(/username/i)
			.type(user.username)
			.findByLabelText(/password/i)
			.type(user.password)
			.findByText(/submit/i)
			.click()
			.findByTestId('username-display')
			.should('have.text', user.username)
			.findByText(/logout/i)
			.click()
			.findByTestId('username-display')
			.should('not.exist')
		})
	  })
	})
	```