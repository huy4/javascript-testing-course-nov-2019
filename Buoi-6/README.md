# Configure Jest for Testing Javascript Applications

## Tổng quan
1. Set a code coverage threshold in Jest to maintain code coverage levels
2. Report Jest Test Coverage to Codecov through TravisCI
3. Use Jest Watch Mode to speed up development
4. Run Jest Watch Mode by default locally with is-ci-cli

## Chi tiết
1. Set a code coverage threshold in Jest to maintain code coverage levels
	- Cài đặt ngưỡng coverage, test sẽ báo fail nếu thấp hơn ngưỡng cài đặt
	- Cài đặt bằng cách cấu hình file jest.config.js
	```
	jest.config.js

	coverageThreshold: {
	  global: {
		statements: 100,
		branches: 100,
		lines: 100,
		functions: 100,
	  },
	  'src/shared/utils.js': {
		statements: 100,
		branches: 100,
		lines: 100,
		functions: 100,
	  }
	}
	```
	- Set global hoặc set cho từng file, nhóm file

2. Report Jest Test Coverage to Codecov through TravisCI
	- Dùng [Travis CI]() để chạy CI
	- [Codecov](https://www.codecov.io/) để report code coverage number và theo dõi số coverage project
	
3. Use Jest Watch Mode to speed up development
	- Chế độ watch của jest cho phép tập trung test những file có thay đổi, bỏ qua những file test không thay đổi so với commit hiện tại
	- Chế độ watch của jest là lựa chọn hay cho quá trình debug bằng dev tools
	- Cách cài đặt:
		```
		package.json
		
		script {
			"test:watch": "jest --watch"
		}
		```
	- Các chế độ trong jest watch
		› Press a to run all tests. // Chạy tất cả các test
		› Press f to run only failed tests. // Chạy những test failed
		› Press p to filter by a filename regex pattern. // Chạy theo những test được filter regex
		› Press t to filter by a test name regex pattern. // Chạy theo những filter test name regex
		› Press q to quit watch mode. // Rời chế độ watch
		› Press Enter to trigger a test run.
		› // Press w để back

4. Run Jest Watch Mode by default locally with is-ci-cli
	- Cài đặt:
		```
		$ npm install --save-dev is-ci-cli
		```
	- Cách chạy CI ở local:
		```
		$ CI=1 npm t
		```

## Đính kèm