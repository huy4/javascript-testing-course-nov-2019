# Javascript Testing Nov 2019

# Introduction

### Planning  

- Finish day: 12/12/2019

------------------------

### Day 1: 21/11/2019

#### Today's progress:
1. Throw an Error with a Simple Test in JavaScript
2. Abstract Test Assertions into a JavaScript Assertion Library
3. Encapsulate and Isolate Tests by building a JavaScript Testing Framework
4. Support Async Tests with JavaScripts Promises through async await
5. Provide Testing Helper Functions as Globals in JavaScript
6. Verify Custom JavaScript Tests with Jest

#### Throughts: 
- Bài học tỉ mỉ nhưng tiết tấu khá nhanh
- Biết cách viết và cách hoạt động function test, expect như jest

#### Link(s) to work:
1. [Buổi 1](./Buoi-1/README.md)

### Day 2: 22/11/2019

#### Today's progress:
1. Lint JavaScript by configuring and running ESLint
2. Format Code by installing and running Prettier
3. Configure Prettier
4. Disable Unnecessary ESLint Stylistic Rules with eslint-config-prettier
5. Validate all files are formatted when linting
6. Avoid Common Errors with Flow Type Definitions
7. Validate Code in a pre-commit git Hook with husky
8. Auto-format all files and validate relevant files in a precommit script with lint-staged

#### Throughts: 
- Khái niệm, cài đặt, cấu hình, cách chạy các tool:
	- ESLint
	- Prettier
	- eslint-config-prettier
	- flow
	- husky
	- lint-staged

#### Link(s) to work:
1. [Buổi 2](./Buoi-2/README.md)

### Day 3: 23/11/2019

#### Today's progress:
1. Override Object Properties to Mock with Monkey-patching in JavaScript
2. Ensure Functions are Called Correctly with JavaScript Mocks
3. Restore the Original Implementation of a Mocked JavaScript Function with jest.spyOn
4. Mock a JavaScript module in a test
5. Make a shared JavaScript mock module

#### Throughts: 
- Khái niệm, mô phỏng, cách dùng của một số jest api:
	- jest.fn()
	- jest.mockImplementation()
	- jest.spyOn()
- Cách mock module, shared mock module

#### Link(s) to work:
1. [Buổi 3](./Buoi-3/README.md)

### Day 4: 25/11/2019

#### Today's progress:
1. Install and run Jest
2. Transpile Modules with Babel in Jest Tests
3. Configure Jest’s test environment for testing node or browser code
4. Support importing CSS files with Jest’s moduleNameMapper
5. Support using webpack CSS modules with Jest
6. Generate a Serializable Value with Jest Snapshots
7. Test an Emotion Styled UI with Custom Jest Snapshot Serializers
8. Handle Dynamic Imports using Babel with Jest

#### Throughts: 
- Cách cấu hình giữa jest, babel và webpack khá khó
- Tìm hiểu thêm được nhiều khái niệm mới có thể áp dụng như bài 3 và bài 6

#### Link(s) to work:
1. [Buổi 4](./Buoi-4/README.md)

### Day 5: 26/11/2019

#### Today's progress:
1. Setup an afterEach Test Hook for all tests with Jest setupTestFrameworkScriptFile
2. Support custom module resolution with Jest moduleDirectories
3. Support a test utilities file with Jest moduleDirectories
4. Step through Code in Jest using the Node.js Debugger and Chrome DevTools
5. Configure Jest to report code coverage on project files
6. Analyze Jest Code Coverage Reports

#### Throughts: 
- Cách cấu hình jest duyệt các module đầu vào được rút gọn không phải là path
- Các bài học cấu hình tạo hứng thú
- Debugger hay nhưng chưa quen dùng

#### Link(s) to work:
1. [Buổi 5](./Buoi-5/README.md)

### Day 6: 27/11/2019

#### Today's progress:
1. Set a code coverage threshold in Jest to maintain code coverage levels
2. Report Jest Test Coverage to Codecov through TravisCI
3. Use Jest Watch Mode to speed up development
4. Run Jest Watch Mode by default locally with is-ci-cli

#### Throughts: 
- Cấu hình CI thông qua TravisCI khá tiện

#### Link(s) to work:
1. [Buổi 6](./Buoi-6/README.md)

### Day 7: 28/11/2019

#### Today's progress:
1. Filter which Tests are Run with Typeahead Support in Jest Watch Mode
2. Run tests with a different configuration using Jest’s --config flag and testMatch option
3. Support Running Multiple Configurations with Jest’s Projects Feature
4. Test specific projects in Jest Watch Mode with jest-watch-select-projects

#### Throughts: 
- Cấu hình jest chia testbase thành nhiều luồng như test cho client hay test cho server để sử dụng đúng môi trường là `nodejs` hay `jsdom`, từ đó có thể tối ưu testbase hơn

#### Link(s) to work:
1. [Buổi 7](./Buoi-7/README.md)

### Day 8: 29/11/2019

#### Today's progress:
1. Run ESLint with Jest using jest-runner-eslint
2. Run only relevant Jest tests on git commit to avoid breakages

#### Throughts: 
- Run ESLint với jest không được dùng nhiều vì config ESLint đã báo lỗi luôn ở IDE.

#### Link(s) to work:
1. [Buổi 8](./Buoi-8/README.md)

### Day 9: 30/11/2019

#### Today's progress:
1. Intro to Test React Components with Jest and React Testing Library
2. Render a React Component for Testing
3. Use Jest DOM for Improved Assertions
4. Use DOM Testing Library to Write More Maintainable React Tests

#### Throughts: 
- Sử dụng DOM Testing Library cung cấp nhiều tùy biến cho function render để hữu ích trong việc tìm kiếm các element trong DOM như cách người dùng sẽ tương tác
- API của DOM dễ đọc và hiểu

#### Link(s) to work:
1. [Buổi 9](./Buoi-9/README.md)

### Day 10: 02/12/2019

#### Today's progress:
1. Use React Testing Library to Render and Test React Components
2. Debug the DOM State During Tests using React Testing Library’s debug Function
3. Test React Component Event Handlers with fireEvent from React Testing Library

#### Throughts: 
- Những nội dung này trước đây đã đọc được

#### Link(s) to work:
1. [Buổi 10](./Buoi-10/README.md)

### Day 11: 03/12/2019

#### Today's progress:
1. Improve Test Confidence with the User Event Module
2. Test prop updates with react-testing-library
3. Assert that something is NOT rendered with react-testing-library
4. Test accessibility of rendered React Components with jest-axe

#### Throughts: 
- Học được thêm các kiến thức mới có thể áp dụng vào testbase như:
	- Sử dụng thư viện `@testing-library/user-event`
	- Test prop updates, test những case render null
	- Sử dụng jest-axe để test code linters

#### Link(s) to work:
1. [Buổi 11](./Buoi-11/README.md)

### Day 12: 04/12/2019

#### Today's progress:
1. Mock HTTP Requests with jest.mock in React Component Tests
2. Mock HTTP Requests with Dependency Injection in React Component Tests
3. Mock react-transition-group in React Component Tests with jest.mock

#### Throughts: 
- Học được thêm cách mock cho codebase chứ HTTP request, mock thư viện
- Chú ý khi mock HTTP request nên ưu tiên sử dụng jest mock api

#### Link(s) to work:
1. [Buổi 12](./Buoi-12/README.md)

### Day 13: 05/12/2019

#### Today's progress:
1. Test componentDidCatch handler error boundaries with react-testing-library
2. Hide console.error Logs when Testing Error Boundaries with jest.spyOn

#### Throughts:
- Học được thêm về `Error Boundaries` và cách để test

#### Link(s) to work:
1. [Buổi 13](./Buoi-13/README.md)

### Day 14: 06/12/2019

#### Today's progress:
1. Ensure Error Boundaries Can Successfully Recover from Errors
2. Use React Testing Library’s Wrapper Option to Simplify using rerender
3. Test Drive the Development of a React Form with React Testing Library
4. Test Drive the Submission of a React Form with React Testing Library

#### Throughts:
- Khi cần test các component từ trạng thái mới về trạng thái ban đầu nên clear hết mock và rerender component
- State component cha sẽ không thay đổi và cũng không phụ thuộc vào thay đổi của component con


#### Link(s) to work:
1. [Buổi 14](./Buoi-14/README.md)

### Day 15: 07/12/2019

#### Today's progress:
1. Test Drive the API Call of a React Form with React Testing Library
2. Test Drive Mocking react-router’s Redirect Component on a Form Submission
3. Test Drive Assertions with Dates in React
4. Use Generated Data in Tests with tests-data-bot to Improve Test Maintainability

#### Throughts:
- Áp dụng Test Drive cho các case API call, react-router’s redirect component, Dates
- Có thể ứng dụng thư viện `tests-data-bot` để generated Data

#### Link(s) to work:
1. [Buổi 15](./Buoi-15/README.md)

### Day 16: 09/12/2019

#### Today's progress:
1. Test Drive Error State with React Testing Library
2. Create a Custom render Function to Simplify Tests of react-router Components
3. Initialize the `history` Object with a Bad Entry to Test the react-router no-match Route
4. Test React Components that Use the react-router Router Provider with createMemoryHistory
5. Write a Custom Render Function to Share Code between Tests and Simplify Tests

#### Throughts:
- Áp dụng custom render function `react-router` để share code giữa các test case vào testbase của linkin-booster
- Test `react-router` đã biết từ trước

#### Link(s) to work:
1. [Buổi 16](./Buoi-16/README.md)

### Day 17: 10/12/2019

#### Today's progress:
1. Test a Redux Connected React Component
2. Test a Redux Connected React Component with Initialized State
3. Create a Custom Render Function to Simplify Tests of Redux Components

#### Throughts:
- Custom render function có thể áp dụng vào testbase của linked-booster

#### Link(s) to work:
1. [Buổi 17](./Buoi-17/README.md)

### Day 18: 11/12/2019

#### Today's progress:
1. Test a Custom React Hook with React’s Act Utility and a Test Component
2. Write a Setup Function to Reduce Duplication of Testing Custom React Hooks
3. Test a Custom React Hook with renderHook from React Hooks Testing Library
4. Test Updates to Your Custom React Hook with rerender from React Hooks Testing Library

#### Throughts:
- Custom render function có thể áp dụng vào testbase của linked-booster

#### Link(s) to work:
1. [Buổi 18](./Buoi-18/README.md)

### Day 19: 12/12/2019

#### Today's progress:
1. Test React Portals with within from React Testing Library
2. Test Unmounting a React Component with React Testing Library
3. Write React Application Integration Tests with React Testing Library

#### Throughts:
- Test React Portals, Application Integration có thể áp dụng vào testbase hiện tại

#### Link(s) to work:
1. [Buổi 19](./Buoi-19/README.md)

### Day 20: 13/12/2019

#### Today's progress:
1. Improve Reliability of Integration Tests using find* Queries from React Testing Library
2. Improve Reliability of Integration Tests Using the User Event Module
3. Install and Run Cypress

#### Throughts:
- Sẽ thử và đưa ra ưu nhược điểm của find* và getBy bằng thực nghiệm
- Cypress rất thú vị

#### Link(s) to work:
1. [Buổi 20](./Buoi-20/README.md)

### Day 21: 14/12/2019

#### Today's progress:
1. Write the First Cypress Test
2. Configure Cypress in cypress.json
3. Installing Cypress Testing Library
4. Script Cypress for Local Development and Continuous Integration
5. Debug a Test with Cypress
6. Test User Registration with Cypress
7. Cypress Driven Development

#### Throughts:
- Test với Cypress thú vị cần đọc thêm nhiều bài để áp dụng vào thực tế

#### Link(s) to work:
1. [Buổi 21](./Buoi-21/README.md)

### Day 22: 16/12/2019

#### Today's progress:
1. Simulate HTTP Errors in Cypress Tests
2. Test User Login with Cypress
3. Create a User with cy.request from Cypress
4. Keep Tests Isolated and Focused with Custom Cypress Commands
5. Use Custom Cypress Command for Reusable Assertions
6. Run Tests as an Authenticated User with Cypress

#### Throughts:
- Cách viết test trực quan dễ hiểu

#### Link(s) to work:
1. [Buổi 22](./Buoi-22/README.md)

### Day 23: 17/12/2019

#### Today's progress:
1. Use cy.request from Cypress to Authenticate as a New User
2. Use a Custom Cypress Command to Login as a User
3. Combine Custom Cypress Commands into a Single Custom Command
4. Install React DevTools with Cypress

#### Throughts:
- Các test dễ hiểu

#### Link(s) to work:
1. [Buổi 23](./Buoi-23/README.md)

### Day 24: 18/12/2019

#### Today's progress:
1. Use DOM Testing Library with React
2. Use DOM Testing Library with jQuery

#### Throughts:
- Cách sử dụng DOM Testing Library đã được học từ bài trước

#### Link(s) to work:
1. [Buổi 24](./Buoi-24/README.md)