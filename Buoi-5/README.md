# JavaScript Mocking Fundamentals

## Tổng quan
9. 

## Chi tiết
9. Setup an afterEach Test Hook for all tests with Jest setupTestFrameworkScriptFile
	- Problem:
		- Thường thì có một vài setup sẽ chạy trước khi test bắt đầu
		```
		import 'react-testing-library/cleanup-after-each'
		import {createSerializer} from 'jest-emotion'
		import * as emotion from 'emotion'

		
		expect.addSnapshotSerializer(createSerializer(emotion))
		```
	- Solution:
		- Thêm file set config
		```
		setup-test.js
		
		import 'react-testing-library/cleanup-after-each'
		import {createSerializer} from 'jest-emotion'
		import * as emotion from 'emotion'
		
		expect.addSnapshotSerializer(createSerializer(emotion))
		```
		
		- Configure jest `setupTestFrameworkScriptFile` (Chạy sau khi Jest loaded)
		```
		jest.config.js
		
		module.exports = {
		  testEnvironment: 'jest-environment-jsdom',
		  moduleNameMapper: {
			'\\.module\\.css$': 'identity-obj-proxy',
			'\\.css$': require.resolve('./test/style-mock.js'),
		  },
		  setupTestFrameworkScriptFile: require.resolve('./test/setup-tests.js'),
		}
		```
		
10. Support custom module resolution with Jest moduleDirectories
	- Problem:
		- Khi import module bằng tên vd: import('calculator-display') thì jest sẽ hiểu đây là load node_modules trong khi thực tế ở đây là đường dẫn
	- Solution:
		- Sử dụng `moduleDirectories` cấu hình giống `resolve` ở webpack
		
		```
		jest.config.js
		
		module.exports = {
		  testEnvironment: 'jest-environment-jsdom',
		  moduleDirectories: ['node_modules', path.join(__dirname, 'src'), 'shared'],
		  moduleNameMapper: {
			'\\.module\\.css$': 'identity-obj-proxy',
			'\\.css$': require.resolve('./test/style-mock.js'),
		  },
		  setupTestFrameworkScriptFile: require.resolve('./test/setup-tests.js')
		}
		```
		
11. Support a test utilities file with Jest moduleDirectories
	- Những testbase có sử dụng utilities ví dụ như render with provider function có thể:
		- Chuyển ra thành các module riêng để có thể sử dụng lại
		```
		function renderWithProviders(ui, options) {
		  return render(<ThemeProvider theme={dark}>{ui}</ThemeProvider>, options)
		}
		```	
		
		- Gom nhiều module lại và sử dụng moduleDirectories để đọc module thay cho path
		```
		test/calculator-test-utils.js
		
		import React from 'react'
		import {render as rtlRender} from 'react-testing-library'
		import {ThemeProvider} from 'emotion-theming'
		import * as themes from 'themes'

		function render(ui, ...rest) {
		  return rtlRender(
			<ThemeProvider theme={themes.dark}>{ui}</ThemeProvider>,
			...rest,
		  )
		}

		export * from 'react-testing-library'
		// override the built-in render with our own
		export {render}
		```
	
		```
		jest.config.js
		
		module.exports = {
		  testEnvironment: 'jest-environment-jsdom',
		  moduleDirectories: [
			'node_modules',
			path.join(__dirname, 'src'),
			'shared',
			path.join(__dirname, 'test'),
		  ],
		  moduleNameMapper: {
			'\\.module\\.css$': 'identity-obj-proxy',
			'\\.css$': require.resolve('./test/style-mock.js'),
		  },
		  setupTestFrameworkScriptFile: require.resolve('./test/setup-tests.js'),
		}
		```
		
12. Step through Code in Jest using the Node.js Debugger and Chrome DevTools
	- Problem:
		- Cách debug bằng console.log không thực sự tuyệt vời
	- Solution:
		- Sử dụng debug của browser là Node.js build-in expect và Chrome DevTools
	- How to use:
		- Đặt cờ là debugger tại nơi muốn debug
		- Thêm script test:debug
		```
		package.json
		
		"scripts": {
			"test:debug": "node --inspect-brk ./node_modules/jest/bin/jest.js --runInBand",
		}
		```
		- Chạy `npm run test:debug`
		- Tại Chrome browser `chrome://inspect` ở mục `Target` chọn `inspect` sẽ mở ra của sổ debugger
		
13. Configure Jest to report code coverage on project files
	- Jest cung cấp code coverage report trong framework
	- Setup:
		```
		package.json
		
		{
		  "name": "calculator",
		  "version": "1.0.0",
		  "description": "See how to configure Jest and Cypress with React, Babel, and Webpack",
		  "main": "index.js",
		  "scripts": {
			"test": "jest --coverage",
			"test:debug": "node --inspect-brk ./node_modules/jest/bin/jest.js --runInBand --watch",
			...
		  },
		  ...
		}
		```
		- Cấu hình để jest report test coverage chạy qua những file mong muốn tại thuộc tính `collectCoverageFrom`
		```
		jest.config.js
		
		module.exports = {
		  testEnvironment: 'jest-environment-jsdom',
		  moduleDirectories: [
			'node_modules',
			path.join(__dirname, 'src'),
			'shared',
			path.join(__dirname, 'test'),
		  ],
		  moduleNameMapper: {
			'\\.module\\.css$': 'identity-obj-proxy',
			'\\.css$': require.resolve('./test/style-mock.js'),
		  },
		  setupTestFrameworkScriptFile: require.resolve('./test/setup-tests.js'),
		  collectCoverageFrom: ['**/src/**/*.js'],
		}
		```
		
14. Analyze Jest Code Coverage Reports
	- Jest sử dụng thư viện `istanbul` để tính code coverage
	- Học được cách jest tính code coverage, cách jest detect các dòng code mà test đi qua

## Đính kèm
	- [Link github](https://github.com/kentcdodds/jest-cypress-react-babel-webpack/tree/egghead-2018/config-jest-14)
			