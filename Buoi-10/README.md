# Test React Components with Jest and React Testing Library

## Tổng quan
1. Use React Testing Library to Render and Test React Components
2. Debug the DOM State During Tests using React Testing Library’s debug Function
3. Test React Component Event Handlers with fireEvent from React Testing Library

## Chi tiết
1. Use React Testing Library to Render and Test React Components
	- Mô phỏng function render trong React Testing Library
	```
	react-testing-library.js

	function render(ui) {
	  const div = document.createElement('div')
	  ReactDOM.render(<FavoriteNumber />, div)
	  getQueriesForElement(div)
	}
	```
	- Sử dụng React Testing Library để sử dụng function render nhanh chóng
	- Sau khi cài thư viện React Testing Library ReactDOM và Testing Library DOM cũng được tự động import
	- Cách dùng React Testing Library
	```
	import {render} from '@testing-library/react'
	```

2. Debug the DOM State During Tests using React Testing Library’s debug Function
	- Function debug in ra log toàn bộ HTML của codebase phần test hiện tại chạy qua trong khi được gọi.
	- Khi muốn in toàn bộ HTML thì không cần sử dụng tham số
	- Muốn in một phần HTML thì thêm tham số vào giống như: debug(input)
	- Cách cài đặt:
		- Import thư viện React Testing Library
	- Cách sử dụng:
	```
	react-testing-library.js

	const {getByLabelText, debug} = render(<FavoriteNumber />)
	debug()
	```
	
3. Test React Component Event Handlers with fireEvent from React Testing Library
	- API `fireEvent` hỗ trợ tất cả những hành vi của người dùng tương tác lên website như change, click, .v..
	- Cách cài đặt
		```
		import {render, fireEvent} from '@testing-library/react'
		```
	- Cách sử dụng:
		```
		test('entering an invalid value shows an error message', () => {
		  const {getByLabelText} = render(<FavoriteNumber />)
		  const input = getByLabelText(/favorite number/i)
		  fireEvent.change(input, {target: {value: '10'}})
		  expect(getByRole('alert')).toHaveTextContent(/the number is invalid/i)
		})
		```