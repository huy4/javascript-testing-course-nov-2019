# Test React Components with Jest and React Testing Library

## Tổng quan
1. Improve Test Confidence with the User Event Module
2. Test prop updates with react-testing-library
3. Assert that something is NOT rendered with react-testing-library
4. Test accessibility of rendered React Components with jest-axe

## Chi tiết
1. Improve Test Confidence with the User Event Module
	- `@testing-library/user-event` được sử dụng để giả lập người dùng tương tác với các element.
	- `@testing-library/user-event` mạnh mẽ hơn, thay thế cho `fireEvent` trong `@testing-library/react`
	- Cách cài đặt:
		- With NPM:
		```
		npm install --save-dev @testing-library/user-event
		```
		- With Yarn:
		```
		yarn add @testing-library/user-event --dev
		```
	- Các sử dụng:
	```
	stat-user-event.js
	
	import React from 'react'
	import user from '@testing-library/user-event'
	import {render} from '@testing-library/react'
	import {FavoriteNumber} from '../favorite-number'

	test('entering an invalid value shows an error message', () => {
	  const {getByLabelText, getByRole} = render(<FavoriteNumber />)
	  const input = getByLabelText(/favorite number/i)
	  user.type(input, '10')
	  expect(getByRole('alert')).toHaveTextContent(/the number is invalid/i)
	})
	```
	- API:
		- click(element)
		- dblClick(element)
		- type(element, text, [options])
		- selectOptions(element, values)

2. Test prop updates with react-testing-library
	- Sử dụng rerender để updates prop của component
	- Ví dụ:
	```
	prop-updates.js
	
	test('entering an invalid value shows an error message', () => {
	  const {getByLabelText, getByRole} = render(<FavoriteNumber />)
	  const input = getByLabelText(/favorite number/i)
	  user.type(input, '10')
	  expect(getByRole('alert')).toHaveTextContent(/the number is invalid/i)
	  debug()
	  <FavoriteNumber max={10} />
	  debug()
	})
	```
	- Rerender sẽ render ra UI mới dựa trên UI gốc với prop thay đổi.
	- Sử dụng rerender để test những kịch bản props updated.

3. Assert that something is NOT rendered with react-testing-library
	- Các query bắt đầu bằng tiền tố `getBy` khi kết quả trả về là lỗi sẽ báo thẳng lỗi dễ dàng debug, được tác giả recommend
	- Các query bát đầu bằng tiền tố `queryBy` khi gặp lỗi, kết quả trả về là `null`

4. Test accessibility of rendered React Components with jest-axe
	- `jest-axe` là một `code linters` giống như eslint hoặc sass-lint, `jest-axe` tìm kiếm những lỗi phổ biến để đảm bảo hệ thống hoạt động bình thường.
	- `jest-axe` cung cấp:
		- Test your interface bằng các công nghệ hỗ trợ mới nhất
		- Kể cả những người dùng không khuyết tật
	- Cách cài đặt:
	```
	npm install --save-dev jest-axe
	```
	
	- Cách sử dụng:
	```
	const { render, cleanup } = require('@testing-library/react')
	const { axe, toHaveNoViolations } = require('jest-axe')
	expect.extend(toHaveNoViolations)

	it('should demonstrate this matcher`s usage with react testing library', async () => {
	  const { container } = render(<App/>)
	  const results = await axe(container)
	  
	  expect(results).toHaveNoViolations()
	  
	  cleanup()
	})
	```
	
	- Hoặc có thể dùng `import 'jest-axe/extend-expect'` để dễ dàng cấu hình automated ở `jest` chạy trước mỗi bài test.

	