# Install, Configure, and Script Cypress for JavaScript Web Applications

## Tổng quan
1. Write the First Cypress Test
2. Configure Cypress in cypress.json
3. Installing Cypress Testing Library
4. Script Cypress for Local Development and Continuous Integration
5. Debug a Test with Cypress
6. Test User Registration with Cypress
7. Cypress Driven Development

## Chi tiết
1. Write the First Cypress Test
	- Run Cypress
	```
	npx cypress open
	```
	- Run app
	```
	npm run dev
	```
	- Thực hiện viết Cypress test giống như tương tác của người dùng lên hệ thống
	```
	describe('anonymous calculator', () => {
	  it('can make calculations', () => {
		cy.visit('http://localhost:8080')
		  .get('._2S_Gj6clvtEi-dZqCLelKb > :nth-child(3)')
		  .click()
		  .get('._1yUJ9HTWYf2v-MMhAEVCAn > :nth-child(4)')
		  .click()
		  .get('._2S_Gj6clvtEi-dZqCLelKb > :nth-child(4)')
		  .click()
		  .get('._1yUJ9HTWYf2v-MMhAEVCAn > :nth-child(5)')
		  .click()
		  .get('.mNQM6vIr72uG0YPP56ow5')
		  .should('have.text', '3')
	  })
	})
	```
	- Đọc Cypress cũng rất dễ hiểu
2. Configure Cypress in cypress.json
	- Cypress được configure tại file cypress.json
	- Các configure có thể tham khảo [tại đây](https://docs.cypress.io/guides/references/configuration.html#Options)
	- Một số cấu hình cơ bản:
	```
	{
	  "baseUrl": "http://localhost:8080",
	  "integrationFolder": "cypress/integration/examples",
	  "viewportHeight": 900,
	  "viewportWidth": 400 
	}
	```
3. Installing Cypress Testing Library
	- Install Cypress Testing Library
	```
	npm install --save-dev @testing-library/cypress
	```
	- Import vào `support/index.js`
	```
	import '@testing-library/cypress/add-commands'
	import './commands'
	```
	- Viết test với Cypress Testing Library
	```
	describe('anonymous calculator', () => {
	  it('can make calculations', () => {
		cy.visit('/')
		  .findByText(/^1$/)
		  .click()
		  .findByText(/^\+$/)
		  .click()
		  .findByText(/^2$/)
		  .click()
		  .findByText(/^=$/)
		  .click()
		  .findByTestId('total')
		  .should('have.text', '3')
	  })
	})
	```
	- Codebase
	```
	function AutoScalingText({children}) {
	  const nodeRef = React.useRef()
	  const scale = getScale(nodeRef.current)
	  return (
		<div
		  className={styles.autoScalingText}
		  style={{transform: `scale(${scale},${scale})`}}
		  ref={nodeRef}
		  data-testid="total"
		>
		  {children}
		</div>
	  )
	}
	```
4. Script Cypress for Local Development and Continuous Integration
	- npm install as a devDependency `start-server-and-test` để khởi động server và test
	```
	npm install --save-dev start-server-and-test
	```
	- Thêm script test vào package.json
	```
	package.json
	
	"cy:run": "cypress run",
	"cy:open": "cypress open",
	"test:e2e": "is-ci \"test:e2e:run\" \"test:e2e:dev\"",
	"test:e2e:run": "start-server-and-test start http://localhost:8080 cy:run",
	"test:e2e:dev": "start-server-and-test dev http://localhost:8080 cy:open",
	"pretest:e2e:run": "npm run build",
	"validate": "npm run test:coverage && npm run test:e2e:run",
	
	"husky": {
	  "hooks": {
		"pre-commit": "lint-staged && npm run test:e2e:run"
	  }
	}
	```
	- Cấu hình travis.yml
	```
	addons: 
	  apt: 
		packages:
		  - libgconf-2-4

	cache: 
	  directories:
		- ~/.npm
		- ~/.cache
	```
5. Debug a Test with Cypress
	- Debug testbase:
		- Debug bằng `promise`
		```
		.findByText(/^2$/)
		.then(subject => {
		  debugger
		  return subject
		})
		```
		- Sử dụng cửa sổ console của Chrome để xem lỗi hiển thị
		![](https://res.cloudinary.com/dg3gyk0gu/image/upload/v1574727281/transcript-images/cypress-debug-a-test-with-cypress-dev-tools-debugger.png)
		- Debug bằng api `pause()`
		- Debug bằng api `debug()`
	- Debug ở source code:
	```
	function App({user, logout}) {
	  ...
	  console.log(theme)
	}
	```
	![](https://res.cloudinary.com/dg3gyk0gu/image/upload/v1574727282/transcript-images/cypress-debug-a-test-with-cypress-theme.png)
6. Test User Registration with Cypress
	- Mock user
	```
	generate.js
	import {build, fake} from 'test-data-bot'

	const buildUser = build('User').fields({
	  username: fake(f => f.internet.userName()),
	  password: fake(f => f.internet.password()),
	})

	export {buildUser}
	```
	- Testbase
	```
	describe('registration', () => {
	  it('should register a new user', () => {
		const user = buildUser()
		cy.visit('/')
		  .findByText(/register/i)
		  .click()
		  .findByLabelText(/username/i)
		  .type(user.username)
		  .findByLabelText(/password/i)
		  .type(user.password)
		  .findByText(/submit/i)
		  .click()
		  .url()
		  .should('eq', `${Cypress.config().baseUtl}/`)
		  .window()
		  .its('localStorage.token')
		  .should('be.a', 'string')
	  })
	})
	```
7. Cypress Driven Development
	- Testbase
	```
	register.js
	
	describe('registration', () => {
	  it('should register a new user', () => {
		const user = buildUser()
		cy.visit('/')
		  .findByText(/register/i)
		  .click()
		  .findByLabelText(/username/i)
		  .type(user.username)
		  .findByLabelText(/password/i)
		  .type(user.password)
		  .findByText(/submit/i)
		  .click()
		  .url()
		  .should('eq', `${Cypress.config().baseUrl}/`)
		  .window()
		  .its('localStorage.token')
		  .should('be.a', 'string')
		  .findByTestId('username-display')
		  .should('have.text', user.username)
	  })
	})
	```
	- Update source code
	```
	app.js
	
	{user ? (
	  <>
		<div data-testid="username-display">{user.username}</div>
		<button type="button" onClick={logout}>
		  Logout
		</button>
	  </>
	  ```
	- Vì Cypress chạy ở chế độ watch nên sau khi update source cần run lại toàn bộ script test để bypass test