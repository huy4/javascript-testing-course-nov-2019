# Test React Components with Jest and React Testing Library

## Tổng quan
1. Test Drive the API Call of a React Form with React Testing Library
2. Test Drive Mocking react-router’s Redirect Component on a Form Submission
3. Test Drive Assertions with Dates in React
4. Use Generated Data in Tests with tests-data-bot to Improve Test Maintainability

## Chi tiết
1. Test Drive the API Call of a React Form with React Testing Library
	- Sử dụng Test Drive test API form cho React Form
	- Các bước thực hiện:
		- Mock HTTP request cho form
		```
		tdd-03-api.js

		import {savePost as mockSavePost} from '../api'
		import {Editor} from '../post-editor-03-api'

		jest.mock('../api')
		
		afterEach(() => {
		  jest.clearAllMocks()
		})
		
		test('renders a form with title, content, tags, and a submit button', () => {
		  mockSavePost.mockResolvedValueOnce()
		  const fakeUser = {id: 'user-1'}
		  const {getByLabelText, getByText} = render(<Editor />)
		  const fakePost = {
			title: 'Test Title',
			content: 'Test content',
			tags: ['tag1', 'tag2'],
		  }

		  getByLabelText(/title/i)
		  getByLabelText(/content/i)
		  getByLabelText(/tags/i)
		  const submitButton = getByText(/submit/i)

		  fireEvent.click(submitButton)

		  expect(submitButton).toBeDisabled()
		  
		  expect(mockSavePost).toHaveBeenCalledWith({
			...fakepost,
			authorId: fakeUser.id,
		  })
		  expect(mockSavePost).toHaveBeenCalledTimes(1)
		}
		```
		- Để test passed thì cần update codebase
		```
		post-editor-03-api.js

		function Editor({user}) {
		  const [isSaving, setIsSaving] = React.useState(false)
		  function handleSubmit(e) {
			e.preventDefault()
			const {title, content, tags} = e.target.elements
			const newPost = {
			  title: title.value,
			  content: content.value,
			  tags: tags.value.split(',').map(t => t.trim()),
			  authorId: user.id,
			}
			setIsSaving(true)
			savePost(newPost)
		  }

		  ...
		}
		```
		
2. Test Drive Mocking react-router’s Redirect Component on a Form Submission
	- Tạo một component có nhiệm vụ chuyển hướng đến trang chủ sử dụng `react-router`
	- Viết Test Drive để implementation codebase đúng
	- Các bước thực hiện:
		- Mock thư viện `react-router` và viết test
		```
		tdd-04-router-redirect.js

		import {Redirect as MockRedirect} from 'react-router'
		
		jest.mock('react-router', () => {
		  return {
			Redirect: jest.fn(() => null),
		  }
		})
		
		test('renders a form with title, content, tags, and a submit button', async () => {
		  ...

		  expect(submitButton).toBeDisabled()

		  expect(mockSavePost).toHaveBeenCalledWith({
			...fakePost,
			authorId: fakeUser.id,
		  })
		  expect(mockSavePost).toHaveBeenCalledTimes(1)

		  await wait(() => expect(MockRedirect).toHaveBeenCalledWith({to: '/'}, {}))
		  expect(MockRedirect).toHaveBeenCalledTimes(1)
		})
		```
		
		- Implementation codebase
		```
		post-editor-04-router-redirect.js

		function Editor({user}) {
		  const [isSaving, setIsSaving] = React.useState(false)
		  const [redirect, setRedirect] = React.useState(false)
		  function handleSubmit(e) {
			e.preventDefault()
			const {title, content, tags} = e.target.elements
			const newPost = {
			  ...
			}
			setIsSaving(true)
			savePost(newPost).then(() => setRedirect(true))
		  }
		  if (redirect) {
			return <Redirect to="/" />
		  }
		  return (
			...
		  )
		}
		```
		
3. Test Drive Assertions with Dates in React
	- Test Drive với Dates là thời gian submit post
	- Các bước thực hiện:
		- Thêm thuộc tính Date vào data gửi lên server
		```
		post-editor-05-dates.js

		function Editor({user}) {
		  const [isSaving, setIsSaving] = React.useState(false)
		  const [redirect, setRedirect] = React.useState(false)
		  function handleSubmit(e) {
			e.preventDefault()
			const {title, content, tags} = e.target.elements
			const newPost = {
			  title: title.value,
			  content: content.value,
			  tags: tags.value.split(',').map(t => t.trim()),
			  date: new Date().toISOString(),
			  authorId: user.id,
			}
		  ...
		}
		```
		
		- Test date gửi lên server
		```
		tdd-05-dates.js

		test('renders a form with title, content, tags, and a submit button', async () => {
		  mockSavePost.mockResolvedValueOnce()
		  const fakeUser = {id: 'user-1'}
		  const {getByLabelText, getByText} = render(<Editor user={fakeUser} />)
		  const fakePost = {
			title: 'Test Title',
			content: 'Test content',
			tags: ['tag1', 'tag2'],
		  }
		  const preDate = new Date().getTime()
		  
		  expect(submitButton).toBeDisabled()

		  expect(mockSavePost).toHaveBeenCalledWith({
			...fakePost,
			date: expect.any(string),
			authorId: fakeUser.id,
		  })
		  expect(mockSavePost).toHaveBeenCalledTimes(1)

		  const postDate = new Date().getTime()
		  const date = mockSavePost.mock.calls[0][0].date).getTime()
		  expect(date).toBeGreaterThanOrEqual(preDate)
		  expect(date).toBeLessThanOrEqual(postDate)
		}
		```
		
4. Use Generated Data in Tests with tests-data-bot to Improve Test Maintainability
	- `tests-data-bot` cho phép generated Data
	- Cách cài đặt
	```
	npm install --save-dev test-data-bot
	```
	- Cách sử dụng:
	```
	import {build, fake, sequence} from 'test-data-bot'

	const postBuilder = build('Post').fields({
	  title: fake(f => f.lorem.words()),
	  content: fake(f => f.lorem.paragraphs().replace(/\r/g, '')),
	  tags: fake(f => [f.lorem.word(), f.lorem.word(), f.lorem.word()]),
	})
	
	const userBuilder = build('User').fields({
	  id: sequence(s => `user-${s}`),
	})
	```