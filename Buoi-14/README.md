# Test React Components with Jest and React Testing Library

## Tổng quan
1. Ensure Error Boundaries Can Successfully Recover from Errors
2. Use React Testing Library’s Wrapper Option to Simplify using rerender
3. Test Drive the Development of a React Form with React Testing Library
4. Test Drive the Submission of a React Form with React Testing Library

## Chi tiết
1. Ensure Error Boundaries Can Successfully Recover from Errors
	- Cần các bài test để đảm bảo sau khi chuyển từ Error Boundaries sang trạng thái ban đầu hoạt động tốt
	- Test như thế nào
		- Sau khi được thay đổi prop thì state hasError của ErrorBoundary thay đổi
		```
		rerender(
			<ErrorBoundary>
			  <Bomb shouldThrow={true} />
			</ErrorBoundary>,
		)
		```
		- Clear mock và rerender lại component để làm mới DOM
		```
		console.error.mockClear()
		mockReportError.mockClear()
  
		rerender(
		  <ErrorBoundary>
			<Bomb />
		  <ErrorBoundary>
		)
		```
		- Thực hiện test với các trường hợp không chứa lỗi
		```
		fireEvent.click(getByText(/try again/i))

		expect(mockReportError).not.toHaveBeenCalled()
		expect(console.error).not.toHaveBeenCalled()
		expect(queryByRole('alert')).not.toBeInTheDocument()
		expect(queryByText(/try again/i)).not.toBeInTheDocument()
		```

2. Use React Testing Library’s Wrapper Option to Simplify using rerender
	- Trong React Testig Library API render có thuộc tính wrapper và truyền vào tham số wrapper
	- Ví dụ:
	```
	const {rerender, getByText, queryByText, getByRole, queryByRole, debug} = render(
		<ErrorBoundary>
		  <Bomb />
		</ErrorBoundary>,
	)
	
	rerender(
		<ErrorBoundary>
		  <Bomb />
		</ErrorBoundary>,
	)
	```
	
	- Thay thế bằng:
	```
	const {rerender, getByText, queryByText, getByRole, queryByRole} = render(
		<Bomb />,
		{wrapper: ErrorBoundary},
	)
	
	rerender(<Bomb />)
	```

3. Test Drive the Development of a React Form with React Testing Library
	- Xây dựng một form sử dụng Test Drive Development và React Testing Library
	- Các bước thực hiện
		- Viết một bài test tổng quá về UI
		```
		test('renders a form with title, content, tags, and a submit button', () => {
		  const {getBYLabelText, getByText} = render(<Editor />)
		  getByLabelText(/title/i)
		  getByLabelText(/content/i)
		  getByLabelText(/tags/i)
		  getByText(/submit/i)
		})
		```
		- Từ bài test implementation codebase để bypass bài test
		```
		function Editor() {
		  return ( 
			<form>
			  <label htmlFor="title-input">Title</label>
			  <input id="title-input" />

			  <label htmlFor="content-input">Content</label>
			  <textarea id="content-input" />

			  <label htmlFor="tags-input">Tags</label>
			  <textarea id="tags-input" />

			  <button type="submit">Submit</button>
			</form>
		  )
		}```
		
4. Test Drive the Submission of a React Form with React Testing Library
	- Sử dụng Test Drive để test form Submission
	- Expect button submit sẽ disabled sau khi được bấm
	```
	test('renders a form with title, content, tags, and a submit button', () => {
		  const {getBYLabelText, getByText} = render(<Editor />)
		  getByLabelText(/title/i)
		  getByLabelText(/content/i)
		  getByLabelText(/tags/i)
		  const submitButton = getByText(/submit/i)
		  
		  fireEvent.click(submitButton)
		  
		  expect(submitButton).toBeDisabled()
		})
	```
	- Thêm codebase để bypass bài test
	```
	function Editor() {
	  const [isSaving, setIsSaving] = React.useState(false)
	  function handleSubmit(e) {
		e.preventDefault()
		setIsSaving(true)
	  }
	  return (
		<form onSubmit={handleSubmit}>
		...
	  )
	}
	```