# JavaScript Mocking Fundamentals

## Tổng quan


## Chi tiết
1. Override Object Properties to Mock with Monkey-patching in JavaScript
	Monkey-patching là một kỹ thuật để thêm, sửa hoặc xóa hành vi mặc định của một đoạn mã mà không thay đổi source code.
	Mock là kỹ thuật cho phép các bài test nhận vào dữ liệu và đảm bảo đưa ra được kết quả như chúng ta mong đợi.
	```
	thumb-war.js
	const utils = require('./utils')

	function thumbWar(player1, player2) {
	  const numberToWin = 2
	  let player1Wins = 0
	  let player2Wins = 0
	  while (player1Wins < numberToWin && player2Wins < numberToWin) {
		const winner = utils.getWinner(player1, player2)
		if (winner === player1) {
		  player1Wins++
		} else if (winner === player2) {
		  player2Wins++
		}
	  }
	  return player1Wins > player2Wins ? player1 : player2
	}

	module.exports = thumbWar
	```
	
	```
	function getWinner(player1, player2) {
	  const winningNumber = Math.random()
	  return winningNumber < 1 / 3
		? player1
		: winningNumber < 2 / 3
		  ? player2
		  : null
	}

	module.exports = {getWinner}
	```
	
	```
	monkey-patching.js
	const assert  = require('assert')
	const thumbWar = require('../thumb-war')
	const utils = require('../utils')
	
	const originalGetWinner = utils.getWinner
	
	// sử dụng kỹ thuật Monkey-patching với hàm getWinner
	// bằng cách mock hàm utils.getWinner trả về p1
	utils.getWinner = (p1, p2) => p1

	const winner = thumbWar('Kent C. Dodds', 'Ken Wheeler')
	assert.strictEqual(winner, 'Kent C. Dodds')
	
	// clean up
	utils.getWinner = originalGetWinner
	```
	
	- `Chú ý`: Sau các bài test cần cleaning up để đảm bảo khi các test khác sử dụng module này ở trạng thái state chưa sửa đổi.

2. Ensure Functions are Called Correctly with JavaScript Mocks
	- Viết các bài test và mock dependencies, chúng ta xác nhận rằng hàm được gọi đúng, vì vậy cần theo dõi số lần gọi function và các tham số truyền vào.
	- Sử dụng hàm `jest.fn` để mock function và kiểm tra số lần gọi function, tham số truyền vào
	- Mock `utils.getWinner = jest.fn((p1, p2) => p1)` thể chỉ định giá trị trả về
	- Test số lần gọi function utils.getWinner bằng `expect(utils.getWinner).toHaveBeenCalledTimes(2)`
	- Test tham số truyền vào `expect(utils.getWinner).toHaveBeenCalledWith('Kent C. Dodds', 'Ken Wheeler')`
	- Test từng lần function hoạt động:
		- Lần 1: `expect(utils.getWinner).toHaveBeenNthCalledWith(1, 'Kent C. Dodds', 'Ken Wheeler')`
		- Lần 2: `expect(utils.getWinner).toHaveBeenNthCalledWith(2, 'Kent C. Dodds', 'Ken Wheeler')`
	- Mô phỏng cách `jest.fn()` hoạt động
	```
	function fn(impl) {
	  const mockFn = (...args) => {
		return impl(...args)
	  }
	  mockFn.mock = {calls: []}
	  mockFn.mockImplementation = newImpl => (impl = newImpl)
	  return mockFn
	}

	const originalGetWinner = utils.getWinner
	utils.getWinner = fn((p1, p2) => p1)
	```
	- Nếu dùng để chỉ định giá trị trả về thì nên dùng `jest.mockImplementation()` (được mô phỏng ở trên)

3. Restore the Original Implementation of a Mocked JavaScript Function with jest.spyOn
	- Mock function làm thay đổi hành vi của function, vì vậy cần clean up sau khi test xong
	- `jest.spyOn` có thể giúp chúng ta làm việc này
	- Mô phỏng cách hoạt động của `jest.spyOn`:
	```
	function spyOn(obj, prop) {
	  const originalValue = obj[prop]
	  obj[prop] = fn()
	  obj[prop].mockRestore = () => (obj[prop] = originalValue)
	}
	```
	
	- Sử dụng `spyOn`:
	```
	test('returns winner', () => {
	  jest.spyOn(utils, 'getWinner')
	  utils.getWinner = jest.fn((p1, p2) => p2)

	  ...

	  // cleanup
	  utils.getWinner.mockRestore()
	}
	```

4. Mock a JavaScript module in a test
	- Monkey-patching hoạt động khi chúng ta sử dụng common JS nhưng trong một số trường hợp của ES module thì không hoạt động.
	- Lúc đó chúng ta cần mock toàn bộ module
	- Cách mock toàn bộ module
	```
	jest.mock('../utils', () => {
	  return {
		getWinner: jest.fn((p1, p2) => p1)
	  }
	})
	```

5. Make a shared JavaScript mock module
	- Thường thì mock các file giống nhau được sử dụng ở nhiều test trong codebase, vì vậy nên share file mock cho các test
	- `Jest` tự động share các file mock trong thư mục `__mocks__`

## Đính kèm
	- [Monkey-patching](https://www.audero.it/blog/2016/12/05/monkey-patching-javascript/)
	- [Link github](https://github.com/kentcdodds/js-mocking-fundamentals)
	- [Jest](https://jestjs.io/docs/)