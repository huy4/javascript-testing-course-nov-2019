# JavaScript Mocking Fundamentals

## Tổng quan
1. Filter which Tests are Run with Typeahead Support in Jest Watch Mode
2. Run tests with a different configuration using Jest’s --config flag and testMatch option
3. Support Running Multiple Configurations with Jest’s Projects Feature
4. Test specific projects in Jest Watch Mode with jest-watch-select-projects

## Chi tiết
1. Filter which Tests are Run with Typeahead Support in Jest Watch Mode
	- `jest-watch-typeahead` làm tăng cường jest watch mode bằng cách thêm các bộ lọc bằng regex để chạy các test theo ý muốn
	- Cách cài đặt
	```
	npm install --save-dev jest jest-watch-typeahead
	```
	```
	jest.config.js
	
	module.exports = {
	  watchPlugins: [
		'jest-watch-typeahead/filename',
		'jest-watch-typeahead/testname',
	  ],
	};
	```
	
2. Run tests with a different configuration using Jest’s --config flag and testMatch option
	- Chia việc test các file client và server thành 2 task riêng biệt với các config riêng, tập file test riêng và môi trường riêng
	- Ở môi trường client sẽ dùng `jsdom`
	- Ở môi trường server sẽ dùng `nodejs`
	- Cách cài đặt:
		- Tạo các file config riêng ví dụ: jest.client.js, jest.server.js
		- Require file jest-common.js vào cả 2 file để sử dụng những config chung
		- Định nghĩa trong từng file config riêng cho server và client

3. Support Running Multiple Configurations with Jest’s Projects Feature
	- Cấu hình file jest.config.js để có thể test nhiều project cùng lúc mà không add thêm nhiều script vào package.json cho từng project (client, server)
	- Cách cài đặt tạo file jest.config.js
	- Require jest-common.js
	- Thuộc tính project để chỉ định các file cấu hình jest cụ thể ứng với các project mà cần chạy test cùng lúc (client, server)
	
4. Test specific projects in Jest Watch Mode with jest-watch-select-projects
	- Thêm plugin `jest-watch-select-projects` để tăng cường jest watch mode
	- Cho phép test riêng biệt test riêng biệt từng project theo cờ P mà không cần viết các câu leenjg rườm rà
	- Cách cài đặt thêm ` jest-watch-select-projects` và thêm cấu hình watchPlugins trong jest-common.js
	```
	jest-common.js
	
	watchPlugins: [
		'jest-watch-typeahead/filename',
		'jest-watch-typeahead/testname',
		'jest-watch-select-projects'
	]
	```
	
## Đính kèm