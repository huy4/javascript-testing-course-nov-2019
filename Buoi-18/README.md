# Test React Components with Jest and React Testing Library

## Tổng quan
1. Test a Custom React Hook with React’s Act Utility and a Test Component
2. Write a Setup Function to Reduce Duplication of Testing Custom React Hooks
3. Test a Custom React Hook with renderHook from React Hooks Testing Library
4. Test Updates to Your Custom React Hook with rerender from React Hooks Testing Library

## Chi tiết
1. Test a Custom React Hook with React’s Act Utility and a Test Component
	- Cách dùng React Testing Library để test react hook
	- Các bước thực hiện:
		- Đặt hook cần test trong một functional component render component
		```
		test('exposes the count and increment/decrement functions', () => {
		  let result
		  function TestComponent() {
			result = useCounter()
			return null
		  }
		})
		```
		- Expect kết quả của hàm theo input
		```
		render(<TestComponent />)
		expect(result.count).toBe(0)
		act(() => result.increment())
		expect(result.count).toBe(1)
		act(() => result.decrement())
		expect(result.count).toBe(0)
		```
		- *Note*: Đặt các hàm của hook trong act() utility, act() dùng để mô phỏng update state của react component
	
2. Write a Setup Function to Reduce Duplication of Testing Custom React Hooks
	- Thực hiện test hook với tham số
	- Xây dựng hàm setup để tránh lặp lại code
	```
	function setup({initialProps} = {}) {
	  const result = {}
	  function TestComponent(props) {
		result.current = useCounter(props)
		return null
	  }
	  render(<TestComponent {...initialProps} />)
	  return result
	}
	```
	- Sử dụng setup function
	```
	test('exposes the count and increment/decrement functions', () => {
	  const result = setup()
	  expect(result.current.count).toBe(0)
	  act(() => result.current.increment())
	  expect(result.current.count).toBe(1)
	  act(() => result.current.decrement())
	  expect(result.current.count).toBe(0)
	})

	test('allows customization of the initial count', () => {
	  const result = setup({initialProps: {initialCount: 3}})
	  expect(result.current.count).toBe(3)
	})
	```
	
3. Test a Custom React Hook with renderHook from React Hooks Testing Library
	- Thay thế setup sử dụng renderHook có sẵn trong `@testing-library/react-hooks`
	- Cách sử dụng
	```
	test('exposes the count and increment/decrement functions', () => {
	  const {result} = renderHook(useCounter)
	  expect(result.current.count).toBe(0)
	  act(() => result.current.increment())
	  expect(result.current.count).toBe(1)
	  act(() => result.current.decrement())
	  expect(result.current.count).toBe(0)
	})

	test('allows customization of the initial count', () => {
	  const {result} = renderHook(useCounter, {initialProps: {initialCount: 3}})
	  expect(result.current.count).toBe(3)
	})

	test('allows customization of the step', () => {
	  const {result} = renderHook(useCounter, {initialProps: {step: 2}})
	  expect(result.current.count).toBe(0)
	  act(() => result.current.increment())
	  expect(result.current.count).toBe(2)
	  act(() => result.current.decrement())
	  expect(result.current.count).toBe(0)
	})
	```
4. Test Updates to Your Custom React Hook with rerender from React Hooks Testing Library
	- Test update react hook sau khi rerender component với prop mới
	- Cách thực hiện:
	```
	test('the step can be changed', () => {
	  const {result, rerender} = renderHook(useCounter, {initialProps: {step: 3}})}
	  expect(result.current.count).toBe(0)
	  act(() => result.current.increment())
	  expect(result.current.count).toBe(3)
	  rerender({step: 2})
	  act(() => result.current.decrement())
	  expect(result.current.count).toBe(1)
	})
	```