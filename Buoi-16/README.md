# Test React Components with Jest and React Testing Library

## Tổng quan
1. Test Drive Error State with React Testing Library
2. Create a Custom render Function to Simplify Tests of react-router Components
3. Initialize the `history` Object with a Bad Entry to Test the react-router no-match Route
4. Test React Components that Use the react-router Router Provider with createMemoryHistory
5. Write a Custom Render Function to Share Code between Tests and Simplify Tests

## Chi tiết
1. Test Drive Error State with React Testing Library
	- Handle Error State
	- Các bước thực hiện:
		- Sử dụng API mockRejectedValueOne() test error state
		- Sử dụng `findByRole` để query role alert
		- Test các case như là: Hiển thị lỗi như mong muốn, button submit không bị disable

		```
		tdd-7-error-state.js
		
		test('renders an error message from the server', async () => {
		  const testError = 'test error'
		  mockSavePost.mockRejectedValueOnce({data: {error: testError}})
		  const fakeUser = userBuilder()
		  const {getByLabelText, getByText, findByRole } = render(<Editor user={fakeUser} />)
		  const submitButton = getByText(/submit/i)

		  fireEvent.click(submitButton)
		  
		  const postError = await findByRole('alert')
		  expect(postError).toHaveTextContent(testError)
		  expect(submitButton).not.toBeDisabled()
		})
		```
		
		- Implementation codebase
		```
		post-editor-07-error-state.js
		
		function Editor({user}) {
		  const [isSaving, setIsSaving] = React.useState(false)
		  const [redirect, setRedirect] = React.useState(false)
		  const [error, setError] = React.useState(null)
		  function handleSubmit(e) {
			e.preventDefault()
			const {title, content, tags} = e.target.elements
			const newPost = {
			  title: title.value,
			  content: content.value,
			  tags: tags.value.split(',').map(t => t.trim()),
			  date: new Date().toISOString(),
			  authorId: user.id,
			}
			setIsSaving(true)
			savePost(newPost).then(
			  () => setRedirect(true),
			  response => {
				setIsSaving(false)
				setError(response.data.error)
			  },
			)
		  }
		  if (redirect) {
			return <Redirect to="/" />
		  }
		  return (
			<form onSubmit={handleSubmit}>
			  <label htmlFor="title-input">Title</label>
			  <input id="title-input" name="title" />

			  <label htmlFor="content-input">Content</label>
			  <textarea id="content-input" name="content" />

			  <label htmlFor="tags-input">Tags</label>
			  <input id="tags-input" name="tags" />

			  <button type="submit" disabled={isSaving}>
				Submit
			  </button>
			  {error ? <div role="alert">{error}</div> : null}
			</form>
		  )
		}

		export {Editor}
		```
	- *Note*: `find*` nên dùng với trường hợp async vì nó sẽ tiếp tục tìm element nhiều lần.
	
2. Write a Custom Render Function to Share Code between Tests and Simplify Tests
	- Problem:
		- Render function được viết lại nhiều lần, khiến testbase khó maintain khi có nhiều người viết test
	- Solution:
		- Custom render function để share giữa các test
		- Render function:
		```
		function renderEditor() {
		  const fakeUser = userBuilder()
		  const utils = render(<Editor user={fakeUser} />)
		  const fakePost = postBuilder()

		  utils.getByLabelText(/title/i).value = fakePost.title
		  utils.getByLabelText(/content/i).value = fakePost.content
		  utils.getByLabelText(/tags/i).value = fakePost.tags.join(', ')
		  const submitButton = utils.getByText(/submit/i)
		  return {
			...utils,
			submitButton,
			fakeUser,
			fakePost,
		  }
		}
		```
		- Sử custom render function ở các test case:
		```
		test('renders a form with title, content, tags, and a submit button', async () => {
		  mockSavePost.mockResolvedValueOnce()
		  const {submitButton, fakePost, fakeUser} = renderEditor()
		  const preDate = new Date().getTime()

		  fireEvent.click(submitButton)

		  expect(submitButton).toBeDisabled()

		  expect(mockSavePost).toHaveBeenCalledWith({
			...fakePost,
			date: expect.any(String),
			authorId: fakeUser.id,
		  })

		  ...
		}
		```
		```
		test('renders an error message from the server', async () => {
		  const testError = 'test error'
		  mockSavePost.mockRejectedValueOnce({data: {error: testError}})
		  const {submitButton, findByRole} = renderEditor()

		  fireEvent.click(submitButton)

		  const postError = await findByRole('alert')
		  expect(postError).toHaveTextContent(testError)
		  expect(submitButton).not.toBeDisabled()
		})
		```
		
3. Test React Components that Use the react-router Router Provider with createMemoryHistory, Write a Custom Render Function to Share Code between Tests and Simplify Tests
	- Problem:
		- Mocking the <Redirect /> component hoạt động nhưng bằng cách này không thể biết được user sẽ điều hướng về đâu
	- Solution:
		- Sử dụng Router với createMemoryHistory để điều hướng component
	- Implementation:
		- Codebase:
		```
		function Main() {
		  return (
			<div>
			  <Link to="/">Home</Link>
			  <Link to="/about">About</Link>
			  <Switch>
				<Route exact path="/" component={Home} />
				<Route path="/about" component={About} />
				<Route component={NoMatch} />
			  </Switch>
			</div>
		  )
		}
		```
		- Testbase:
		- Import `Router` và `createMemoryHistory`
		```
		import {Router} from 'react-router-dom'
		import {createMemoryHistory} from 'history'
		```
		- Khởi tạo định tuyến component ở Home page với `createMemoryHistory`
		- Render Main component với context Router component
		- Test các điều hướng khi user tương tác với header
		```
		test('main renders about and home and I can navigate to those pages', () => {
		  const history = createMemoryHistory({initialEntries: ['/']})
		  const {getByRole, getByText} = render(
			<Router history={history}>
			  <Main />
			</Router>,
		  )
		  expect(getByRole('heading')).toHaveTextContent(/home/i)
		  fireEvent.click(getByText(/about/i))
		  expect(getByRole('heading')).toHaveTextContent(/about/i)
		})
		```
		
4. Initialize the `history` Object with a Bad Entry to Test the react-router no-match Route, Test React Components that Use the react-router Router Provider with createMemoryHistory, Write a Custom Render Function to Share Code between Tests and Simplify Tests
	- Test trường hợp user enters a bad URL hoặc routing error
	- Implementation:
		- Codebase:
		```
		const NoMatch = () => (
		  <div>
			<h1>404</h1>
			<p>No match</p>
		  </div>
		)
		
		function Main() {
		  return (
			<div>
			  <Link to="/">Home</Link>
			  <Link to="/about">About</Link>
			  <Switch>
				<Route exact path="/" component={Home} />
				<Route path="/about" component={About} />
				<Route component={NoMatch} />
			  </Switch>
			</div>
		  )
		}
		```
		- Testbase:
		- Cấu hình `createMemoryHistory` với bad URL
		- Test xem có hiển thị ra component NoMatch không 
		```
		test('landing on a bad page shows no match component', () => {
		  const history = createMemoryHistory({
			initialEntries: ['/something-that-does-not-match'],
		  })
		  const {getByRole, debug} = render(
			<Router history={history}>
			  <Main />
			</Router>,
		  )
		})
		
		expect(getByRole('heading')).toHaveTextContent(/404/i)
		```
	
5. Create a Custom render Function to Simplify Tests of react-router Components
	- Custom render function
	```
	react-router-3.js
	
	import {render as rtlRender, fireEvent} from '@testing-library/react'
	
	function render(
	  ui,
	  {
		route = '/',
		history = createMemoryHistory({initialEntries: [route]}),
		...renderOptions
	  } = {},
	) {
	  function Wrapper({children}) {
		return <Router history={history}>{children}</Router>
	  }
	  return {
		...rtlRender(ui, {
		  wrapper: Wrapper,
		  ...renderOptions,
		}),
		history,
	  }
	}
	```
	- Cách sử dụng
	```
	react-router-3.js
	
	test('main renders about and home and I can navigate to those pages', () => {
	  const {getByRole, getByText} = render(<Main />)
	  expect(getByRole('heading')).toHaveTextContent(/home/i)
	  fireEvent.click(getByText(/about/i))
	  expect(getByRole('heading')).toHaveTextContent(/about/i)
	})

	test('landing on a bad page shows no match component', () => {
	  const {getByRole} = render(<Main />, {
		route: '/something-that-does-not-match',
	  })
	  expect(getByRole('heading')).toHaveTextContent(/404/i)
	})
	```