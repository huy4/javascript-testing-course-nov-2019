# JavaScript Mocking Fundamentals

## Tổng quan
1. Install and run Jest
2. Transpile Modules with Babel in Jest Tests
3. Configure Jest’s test environment for testing node or browser code
4. Support importing CSS files with Jest’s moduleNameMapper
5. Support using webpack CSS modules with Jest
6. Generate a Serializable Value with Jest Snapshots
7. Test an Emotion Styled UI with Custom Jest Snapshot Serializers
8. Handle Dynamic Imports using Babel with Jest

## Chi tiết
1. Install and run Jest
	- Install Jest testing framework with `npm install --save-dev jest`
	- Thêm `jest` vào `npm run test` và `npm run validate` ở `package.json`
	- Jest sẽ chạy qua những file thuộc thư mục `__tests__` hoặc có đuổi `**.spec` hoặc `*.test.js`

2. Transpile Modules with Babel in Jest Tests
	- Problem:
		- ES modules are not supported in Node
		- Bởi vì Jest chạy với Node, khi nó gặp câu lệnh import, sẽ báo syntax error.
	- Solution:
		- Sử dụng Babel configured để có thể transpile modules
		- Cài đặt Node environment isTest 
		```
		.babelrc.js
		
		const isTest = String(process.env.NODE_ENV) === 'test'
		
		presets: [
			['@babel/preset-env', {modules: isTest ? 'commonjs' : false}],
		```
	- Note: Jest có thể tự động nhận `babelrc` và chạy với config của babel
3. Configure Jest’s test environment for testing node or browser code
	- `Jest` có thể chạy trên môi trường browser hoặc node
	- Môi trường mặc định của Jest là môi trường giống như browser thông qua `jsdom`
	- `jsdom` mô phỏng một môi trường browser mà không cần chạy bất cứ thứ gì ngoài JavaScript đơn thuần
	- Có thể lựa chọn môi trường node
	- Về tốc độ thì node sẽ nhanh hơn jsdom`
	```
	testEnvironment [String]
	default: "jsdom"
	String: "jsdom" || "node"
	```
	
	- Cách cấu hình:
	```
	jest.config.js
	
	module.exports = {
        testEnvironment: 'jest-environment-node',
    }
	```
	
4. Support importing CSS files with Jest’s moduleNameMapper
	- Problem:
		- Mặc định Jest không xử lý được style mà phải [thông qua webpack](https://jestjs.io/docs/en/webpack) bằng cách cấu hình sử dụng `moduleNameMapper`
		- `moduleNameMapper` là cầu nối giữa Jest và webpack
	- Solution
		- Tạo một file mock
		```
		style-mock.js
		
		module.exports = {}
		```

		- Sử dụng `moduleNameMapper` [object<string, string>]
		```
		jest.config.js
		
		module.exports = {
			testEnvironment: 'jest-environment-jsdom',
			moduleNameMapper: {
				'\\.css$': require.resolve('./test/style-mock.js'),
			}
		}
		```

5. Support using webpack CSS modules with Jest
	- Problem
		- Khi sử dụng empty module `(module.exports = {})` sẽ trả về undefind khi expected className
	- Solution
		- Sử dụng thư viện [identity-obj-proxy](https://github.com/keyanzhang/identity-obj-proxy#readme) để tạo mock CSS module
		- Thư viện sẽ trả về className mong muốn.
		```
		jest.config.js
		
		module.exports = {
		  testEnvironment: 'jest-environment-jsdom',
		  moduleNameMapper: {
			'\\.module\\.css$': 'identity-obj-proxy',
			'\\.css$': require.resolve('./test/style-mock.js'),
		  },
		}
		```
6. Generate a Serializable Value with Jest Snapshots
	- Snapshots được xem như một phần của quy trình review code
	- Jest sẽ snapshot đưa ra kết quả dễ đọc để keep track và so sánh trong lần review code tiếp theo
	- Snapshots thông qua api `toMatchSnapshot()`
	- Trong lần test tiếp theo, jest sẽ so sánh đầu ra và snapshot trước đó
		- Nếu trùng khớp bài test sẽ pass
		- Nếu không thì sẽ fix code hoặc update snapshot bằng câu lệnh `npm run test -- -u`
	
7. Test an Emotion Styled UI with Custom Jest Snapshot Serializers
	- Problem:
		- Snapshot chỉ đưa ra className
		- Khi thay đổi một style ví dụ color: white sang blue thì terminal chỉ báo lỗi ở className rất khó để debug
	- Solution:
		- Sử dụng library `jest-emotion`
		```
		Terminal
		
		$ npm install --save-dev jest-emotion
		```

		- Cách dùng
		```
		import {createSerializer} from 'jest-emotion'
		import * as emotion from 'emotion'
		import CalculatorDisplay from '../calculator-display'

		expect.addSnapshotSerializer(createSerializer(emotion))

		test('mounts', () => {
		  const {container} = render(<CalculatorDisplay value="0" />)
		  expect(container.firstChild).toMatchSnapshot()
		})
		```
		
8. Handle Dynamic Imports using Babel with Jest
	- ES modules trong ES6 là cách để chia codebase thành các module
	- Có 2 kiểu import trong ES modules là: Static import và dynamic import
		- Static import là cách load các module ban đầu
		- Dynamic import là cách load các module khi cần
	- Problem:
		- Node không support dynamic import
	- Solution:
		- Cài đặt babel plugin vào devDependencies có tên `babel-plugin-dynamic-import-node`
		- Tiếp theo là config Babel
		```
		.babelrc.js
		
		isTest ? 'babel-plugin-dynamic-import-node' : null
		```
	