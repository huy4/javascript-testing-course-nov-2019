# User DOM Testing Library to test any JS framework

## Tổng quan
1. Use DOM Testing Library with React
2. Use DOM Testing Library with jQuery

## Chi tiết
1. Use DOM Testing Library with React
	- Write custom render function
	- React binds all event handlers to the body we’ll have to append our container to the body
	- Add a cleanup method to unmount the component and cleanup the DOM
	```
	function render(ui) {
	  const container = document.createElement('div')
	  ReactDOM.render(ui, container)
	  document.body.appendChild(container)
	  return {
		...getQueriesForElement(container),
		container,
		cleanup() {
		  document.body.removeChild(container)
		}
	  }
	}
	```
2. Use DOM Testing Library with jQuery
	- Apply our jQuery plugin to that DOM node
	```
	jquery.test.js
	
	test('counter increments', () => {
	  const div = document.createElement('div')
	  $(div).countify()
	  const {getByText} = getQueriesForElement(div)
	  const counter = getByText('0')
	  fireEvent.click(counter)
	  expect(counter).toHaveTextContent('1')

	  fireEvent.click(counter)
	  expect(counter).toHaveTextContent('2')
	})
	```