# Test React Components with Jest and React Testing Library

## Tổng quan
1. Mock HTTP Requests with jest.mock in React Component Tests
2. Mock HTTP Requests with Dependency Injection in React Component Tests
3. Mock react-transition-group in React Component Tests with jest.mock

## Chi tiết
1. Mock HTTP Requests with jest.mock in React Component Tests
	- Chúng ta có thể dùng jest mock API để mock HTTP requests
	- Import api, gọi jest.mock() nhận api làm tham số, sử dụng các thuộc tính như mockResolvedValueOnce để nhận về kết quả
	```
	http-jest-mock.js
	
	import React from 'react'
	import {render, fireEvent, wait} from '@testing-library/react'
	import {loadGreeting as mockLoadGreeting} from '../api'
	import {GreetingLoader} from '../greeting-loader-01-mocking'

	jest.mock('../api')
	```
	- Mục tiêu test bao gồm
		- Hành vi của lời gọi api (tham số truyền vào, gọi bao nhiêu lần)
		- Kết quả trả về hiển thị lên UI
	- *Chú ý*: Đối với test action như:
	```
	expect(getByLabelText(/greeting/i)).toHaveTextContent(testGreeting)
	```
	- Thì cần sử dụng [wait](https://testing-library.com/docs/dom-testing-library/api-async#wait)
	- Ví dụ:
	```
	http-jest-mock.js
	
	import React from 'react'
	import {render, fireEvent, wait} from '@testing-library/react'
	import {loadGreeting as mockLoadGreeting} from '../api'
	import {GreetingLoader} from '../greeting-loader-01-mocking'

	jest.mock('../api')

	test('loads greetings on click', async () => {
	  const testGreeting = 'TEST_GREETING'
	  mockLoadGreeting.mockResolvedValueOnce({data: {greeting: testGreeting}})
	  const {getByLabelText, getByText} = render(<GreetingLoader />)
	  const nameInput = getByLabelText(/name/i)
	  const loadButton = getByText(/load/i)
	  nameInput.value = 'Mary'
	  fireEvent.click(loadButton)
	  expect(mockLoadGreeting).toHaveBeenCalledWith('Mary')
	  expect(mockLoadGreeting).toHaveBeenCalledTimes(1)
	  await wait(() =>
		expect(getByLabelText(/greeting/i)).toHaveTextContent(testGreeting),
	  )
	})
	```
2. Mock HTTP Requests with Dependency Injection in React Component Tests
	- Phương pháp jest mock API có thể trong một số trường hợp sẽ không hoạt động ví dụ như Storybook
	- Có thể sử dụng phương pháp thay thế là Dependency Injection
	- Dependency Injection là cách để truyền các Dependency là mock function để truyền vào Component
	- Ví dụ:
	```
	http-jest-mock.js
	
	import React from 'react'
	import {render, fireEvent, wait} from '@testing-library/react'
	import {GreetingLoader} from '../greeting-loader-01-mocking'


	test('loads greetings on click', async () => {
	  const mockLoadGreeting = jest.fn()
	  const testGreeting = 'TEST_GREETING'
	  mockLoadGreeting.mockResolvedValueOnce({data: {greeting: testGreeting}})
	  const {getByLabelText, getByText} = render(<GreetingLoader loadGreeting={mockLoadGreeting} />)
	  const nameInput = getByLabelText(/name/i)
	  const loadButton = getByText(/load/i)
	  nameInput.value = 'Mary'
	  fireEvent.click(loadButton)
	  expect(mockLoadGreeting).toHaveBeenCalledWith('Mary')
	  expect(mockLoadGreeting).toHaveBeenCalledTimes(1)
	  await wait(() =>
		expect(getByLabelText(/greeting/i)).toHaveTextContent(testGreeting),
	  )
	})
	```
	
	```
	greeting-loader-01-mocking.js
	
	import React from 'react'
	import * as api from './api'

	function GreetingLoader({loadGreeting = api.loadGreeting}) {
	  const [greeting, setGreeting] = React.useState('')
	  async function loadGreetingForInput(e) {
		e.preventDefault()
		const {data} = await loadGreeting(e.target.elements.name.value)
		setGreeting(data.greeting)
	  }
	  return (
		<form onSubmit={loadGreetingForInput}>
		  <label htmlFor="name">Name</label>
		  <input id="name" />
		  <button type="submit">Load Greeting</button>
		  <div aria-label="greeting">{greeting}</div>
		</form>
	  )
	}

	export {GreetingLoader}
	```
	
3. Mock react-transition-group in React Component Tests with jest.mock
	- `react-transition-group` là thư viện để tạo những hiệu ứng trong quá trình transition
	- Mục tiêu test:
		- Kiểm tra component khi chưa, giữa và sau khi tương tác
	- Cách cài đặt
	```
	mock-component.js
	
	import React from 'react'
	import {render, fireEvent} from '@testing-library/react'
	import {HiddenMessage} from '../hidden-message'

	jest.mock('react-transition-group', () => {
	  return {
		CSSTransition: props => (props.in ? props.children : null),
	  }
	})

	test('shows hidden message when toggle is clicked', () => {
	  const myMessage = 'hello world'
	  const {getByText, queryByText} = render(
		<HiddenMessage myMessage={myMessage} />,
	  )
	  const toggleButton = getByText(/toggle/i)
	  expect(queryByText(myMessage)).not.toBeInTheDocument()
	  fireEvent.click(toggleButton)
	  expect(getByText(myMessage)).toBeInTheDocument()
	  fireEvent.click(toggleButton)
	  expect(queryByText(myMessage)).not.toBeInTheDocument()
	})
	```
	- *Chú ý*: Nếu không sử dụng mock cho thư viện `react-transition-group` thì có thể sử dụng wait trong @testing-library/react vì nó xử lý được hành động cần thời gian xử lý