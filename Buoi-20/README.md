# Test React Components with Jest and React Testing Library

## Tổng quan
1. Improve Reliability of Integration Tests using find* Queries from React Testing Library
2. Improve Reliability of Integration Tests Using the User Event Module
3. Install and Run Cypress

## Chi tiết
1. Improve Reliability of Integration Tests using find* Queries from React Testing Library
	- Sử dụng query bằng find* thay vì sử dụng getBy vì find có thể tìm kiếm element theo cơ chế asynchronous, tức là element sẽ được tìm kiếm nhiều lần thay vì một lần như getBy, việc tìm kiếm lặp lại nhiều lần nếu cây DOM có thay đổi
	- Sử dụng find* tăng thời gian query lên nhưng không đáng kể nên query vẫn rất nhanh

2. Improve Reliability of Integration Tests Using the User Event Module
	- Sử dụng User Event module thay fireEvent có trong react-testing-library
	- User Event giúp code dễ maintain hơn, mô phỏng được nhiều hành vi của người dùng hơn so với fireEvent

3. Install and Run Cypress
	- Install `Cypress`
	```
	npm install --save-dev cypress
	```
	- Run `Cypress`
	```
	npx cypress open
	```
	- Cấu hình `eslint`
	```
	module.exports = {
	  root: true,
	  plugins: ['eslint-plugin-cypress'],
	  extends: ['kentcdodds', 'kentcdodds/import', 'plugin:cypress/recommended'],
	  env: {'cypress/globals': true},
	}
	```
	- `.gitignore`
	```
	dist
	node_modules
	coverage
	cypress/videos
	cypress/screenshots
	```