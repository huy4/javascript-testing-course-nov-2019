# Test React Components with Jest and React Testing Library

## Tổng quan
1. Test a Redux Connected React Component
2. Test a Redux Connected React Component with Initialized State
3. Create a Custom Render Function to Simplify Tests of Redux Components

## Chi tiết
1. Test a Redux Connected React Component
	- Codebase
	```
	redux-reducer.js

	const initialState = {count: 0}
	function reducer(state = initialState, action) {
	  switch (action.type) {
		case 'INCREMENT':
		  return {
			count: state.count + 1,
		  }
		case 'DECREMENT':
		  return {
			count: state.count - 1,
		  }
		default:
		  return state
	  }
	}

	export {reducer}
	```
	```
	redux-counter.js
	
	import React from 'react'
	import {useSelector, useDispatch} from 'react-redux'

	function Counter() {
	  const count = useSelector(state => state.count)
	  const dispatch = useDispatch()
	  const increment = () => dispatch({type: 'INCREMENT'})
	  const decrement = () => dispatch({type: 'DECREMENT'})
	  return (
		<div>
		  <h2>Counter</h2>
		  <div>
			<button onClick={decrement}>-</button>
			<span aria-label="count">{count}</span>
			<button onClick={increment}>+</button>
		  </div>
		</div>
	  )
	}

	export {Counter}
	```
	- Testbase
	- Đối với react connected component cần test với context là Provider với store được mock sẵn
	```
	redux-01.js
	
	import {Counter} from '../redux-counter'
	import {store} from '../redux-store'

	test('can render with redux with defaults', () => {
	  const {getByLabelText, getByText} = render(
		<Provider store={store}>
		  <Counter />
		</Provider>,
	  )
	  fireEvent.click(getByText('+'))
	  expect(getByLabelText(/count/i)).toHaveTextContent('1')
	})
	```
	
2. Test a Redux Connected React Component with Initialized State
	- Initialized state ở store bằng cách
	```
	 const store = createStore(reducer, {count: 3})
	```
	- Viết test cho react connected component bằng Initialized state
	```
	redux-02.js
	
	import {createStore} from 'redux'
	import {store as appStore} from '../redux-store'
	import {reducer} from '../redux-reducer'
	
	test('can render with redux with custom initial state', () => {
	  const store = createStore(reducer, {count: 3})
	  const {getByLabelText, getByText} = render(
		<Provider store={store}>
		  <Counter />
		</Provider>,
	  )
	  fireEvent.click(getByText('-'))
	  expect(getByLabelText(/count/i)).toHaveTextContent('2')
	})
	```
	
3. Create a Custom Render Function to Simplify Tests of Redux Components
	- Custom render function
	```
	function render(
	  ui, 
	  {initialState, store = createStore(reducer, initialState), ...rtlOptions} = {}) {
	  function Wrapper({children}) {
		return <Provider store={store}>{children}</Provider>
	  }
	  return {
		...rtlRender(ui, {wrapper: Wrapper, ...rtlOptions}),
		store,
	  }
	}
	```