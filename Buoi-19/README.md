# Test React Components with Jest and React Testing Library

## Tổng quan
1. Test React Portals with within from React Testing Library
2. Test Unmounting a React Component with React Testing Library
3. Write React Application Integration Tests with React Testing Library

## Chi tiết
1. Test React Portals with within from React Testing Library
	- Codebase
	```
	modal.js
	
	import React from 'react'
	import ReactDOM from 'react-dom'

	let modalRoot = document.getElementById('modal-root')
	if (!modalRoot) {
	  modalRoot = document.createElement('div')
	  modalRoot.setAttribute('id', 'modal-root')
	  document.body.appendChild(modalRoot)
	}

	// don't use this for your modals.
	// you need to think about accessibility and styling.
	// Look into: https://ui.reach.tech/dialog
	function Modal({children}) {
	  const el = React.useRef(document.createElement('div'))
	  React.useLayoutEffect(() => {
		const currentEl = el.current
		modalRoot.appendChild(currentEl)
		return () => modalRoot.removeChild(currentEl)
	  }, [])
	  return ReactDOM.createPortal(children, el.current)
	}

	export {Modal}
	```
	- Để test React Portal cần render React Portal trong một DOM tree
	```
	portals.js
	
	import React from 'react'
	import {render} from '@testing-library/react'
	import {Modal} from '../modal'

	test('modal shows the children', () => {
	  const {getByTextId} = render(
		<>
		  <div data-testid="foo" />
		  <Modal>
			<div dta-testid="test" />
		  </Modal>
		</>
		{baseElement: document.getElementById('modal-root')}
	  )
	  queries.getByTestId(document.body, 'foo')
	  expect(getByTestId('test')).toBeInTheDocument()
	})
	```

2. Test Unmounting a React Component with React Testing Library
	- Khi giá trị trả về từ useEffect, useLayoutEffect or the componentWillUnmount, React component cần cleanup
	- Mock console.error và restore sau khi dùng
	```
	beforeAll(() => {
		jest.spyOn(console, 'error').mockImplementation(() => {})
	})

	afterAll(() => {
	  console.error.mockRestore()
	})

	beforeEach(() => {
	  jset.clearAllMocks()
	})
	
	afterEach(() => {
	  jest.clearAllMocks()
	  jest.useRealTimers()
	})
	```
	- Kịch bản test
	```
	test('does not attempt to set state when unmounted (to prevent memory leaks)', () => {
	  jest.useFakeTimers()
	  const {unmount} = render(<Countdown />)
	  unmount()
	  act(() => jest.runOnlyPendingTimers())
	  expect(console.error).not.toHaveBeenCalled()
	})
	```
	
3. Write React Application Integration Tests with React Testing Library
	- Test app với nhiều page tương tác qua lại
	- Test theo hành vi người dùng tương tác app
		- Mock HTTP request
		```
		import React from 'react'
		import {render, fireEvent} from '@testing-library/react'
		import {submitForm as mockSubmitForm} from '../api'
		import App from '../app'

		jest.mock('../api')
		
		test('Can fill out a form across multiple pages', async () => {
		  mockSubmitForm.mockResolveValueOnce({success: true})
		  const testDAta = {food: 'test food', drink: 'test drink'}
		  render(<App />)
		})
		```
		- Tương tác với từng form trong app
		```
		fireEvent.click(getByText(/fill.*form/i))
		fireEvents.change(getByLabelText(/food/i), {target: {value: testData.food}})
		
		fireEvent.click(getByText(/next/i))
		fireEvents.change(getByLabelText(/drink/i), {target: {value: testData.drink}})
		
		fireEvent.click(getByText(/review/i))
		
		expect(getByLabelText(/food/i)).toHaveTextContent(testData.food)
		
		expect(getByLabelText(/drink/i)).toHaveTextContent(testData.drink)
		
		fireEvent.click(getByText(/confirm/i, {selector: 'button'}))
		
		expect(mockSubmitForm).toHaveBeenCalledWith(testData)
		expect(mockSubmitForm).toHaveBeenCalledTimes(1)

		fireEvent.click(await findByText(/home/i))

		expect(getByText(/welcome home/i)).toBeInTheDocument()
		```
