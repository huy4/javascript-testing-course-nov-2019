# Fundamentals of Testing in JavaScript

## Tổng quan

1. Throw an Error with a Simple Test in JavaScript
2. Abstract Test Assertions into a JavaScript Assertion Library
3. Encapsulate and Isolate Tests by building a JavaScript Testing Framework
4. Support Async Tests with JavaScripts Promises through async await
5. Provide Testing Helper Functions as Globals in JavaScript
6. Verify Custom JavaScript Tests with Jest

## Chi tiết

1. Throw an Error with a Simple Test in JavaScript
	- Bài đưa ra các kiến thức cơ bản về một bài test tự động trong JavaScript
	- Một test gặp lỗi khi kết quả nhận được không trùng với kết quả mong đợi
	
	```
	math.js

	const sum = (a, b) => a + b
	const subtract = (a, b) => a + b
	
	result = subtract(7, 3)
	expected = 4
	
	if (result !== expected){
		throw new Error(`${result} is not equal to ${expected}`)
	}
	```
	
2. Abstract Test Assertions into a JavaScript Assertion Library
	- Viết một Test Assertions để việc viết test dễ hiểu hơn và tránh lỗi lặp
	- Thực tế Test Assertions đã được hỗ trợ bởi `jest`
	
	```
	assertion-library.js

	const {sum, subtract} = require('../math')

	result = sum(3, 7)
	expected = 10
	
	function expect(actual) {
	  return {
		toBe(expected) {
		  if (actual !== expected) {
			throw new Error(`${actual} is not equal to ${expected}`)
		  }
		},
		toEqual(expected) {},
	  }
	}
	
	expect(result).toBe(expected)
	```

3. Encapsulate and Isolate Tests by building a JavaScript Testing Framework
	- Một vấn đề quan trọng trong việc viết test là báo lỗi cho developer debug
	- Xây dựng một function test để
		- Gói gọn việc viết test
		- Độc lập chúng với các test khác trong file
		- Khi chạy tất cả các test thì việc báo lỗi sẽ tốt hơn
	
	```
	testing-framework.js

	function test(title, callback) {
	  try {
		callback()
		console.log(`✓ ${title}`)
	  } catch (error) {
		console.error(`✕ ${title}`)
		console.error(error)
	  }
	}
	
	function sumTest(){
		result = sum(3, 7)
		expected = 10
		expect(result).toBe(expected)
	}
	
	function subtractTest(){
		result = subtract(7,3)
		expected = 4
		expect(result).toBe(expected)
	}
	
	test('sum adds numbers', sumTest)
	test('subtract subtracts numbers', subtractTest)
	```

4. Support Async Tests with JavaScripts Promises through async await
	- Hàm test được viết hoạt động tốt với synchronous test

	```
	async-await.js
	
	test('sumAsync adds numbers asynchronously', async () => {
	  const result = await sumAsync(3, 7)
	  const expected = 10
	  expect(result).toBe(expected)
	})

	test('subtractAsync subtracts numbers asynchronously', async () => {
	  const result = await subtractAsync(7, 3)
	  const expected = 4
	  expect(result).toBe(expected)
	})
	
	async function test(title, callback) {
	  try {
		await callback()
		console.log(`✓ ${title}`)
	  } catch (error) {
		console.error(`✕ ${title}`)
		console.error(error)
	  }
	}
	
	```
	
5. Provide Testing Helper Functions as Globals in JavaScript
	- Chuyển functions test và expect thành object để có thể dùng cho cả app

	```
	setup-globals.js
	
	async function test(title, callback) {
	  try {
		await callback()
		console.log(`✓ ${title}`)
	  } catch (error) {
		console.error(`✕ ${title}`)
		console.error(error)
	  }
	}

	function expect(actual) {
	  return {
		toBe(expected) {
		  if (actual !== expected) {
			throw new Error(`${actual} is not equal to ${expected}`)
		  }
		}
	  }
	}

	global.test = test
	global.expect = expect
	```
	
	Run with
	
	```
	node --require ./setup-globals.js lessons/globals.js
	```

6. Verify Custom JavaScript Tests with Jest
	- Terminal
		
		```
		npx jest
		```
	
		```
		jest.test.js
		
		const {sumAsync, subtractAsync} = require('../math')

		test('sumAsync adds numbers asynchronously', async () => {
		  const result = await sumAsync(3, 7)
		  const expected = 10
		  expect(result).toBe(expected)
		})

		test('subtractAsync subtracts numbers asynchronously', async () => {
		  const result = await subtractAsync(7, 3)
		  const expected = 4
		  expect(result).toBe(expected)
		})
		```

## Đính kèm

5. Provide Testing Helper Functions as Globals in JavaScript
Link to github: https://github.com/kentcdodds/js-testing-fundamentals/tree/egghead-2018/lessons/05-provide-helper-functions-as-globals