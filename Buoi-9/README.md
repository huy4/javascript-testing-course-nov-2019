# Test React Components with Jest and React Testing Library

## Tổng quan
1. Intro to Test React Components with Jest and React Testing Library
2. Render a React Component for Testing
3. Use Jest DOM for Improved Assertions
4. Use DOM Testing Library to Write More Maintainable React Tests

## Chi tiết
1. Intro to Test React Components with Jest and React Testing Library
	- Tác giả không khuyến khích cài đặt các bài test cụ thể
	- Các bài test được linh động, được cài đặt mô phỏng theo tương tác của người dùng với hệ thống

2. Render a React Component for Testing
	- Sử dụng ReactDOM để render những React Component đơn giản
	- Ví dụ
	```
	react-dom.js

	import React from 'react'
	import {FavoriteNumber} from '../favorite-number'
	
	test('renders a number input with a label "Favorite Number"', () => {
	  const div = document.createElement('div')
	  ReactDOM.render(<FavoriteNumber />, div)
	  expect(div.querySelector('input').type).toBe('number')
	  expect(div.querySelector('label').textContent).toBe('Favorite Number'))
	})
	```

3. Use Jest DOM for Improved Assertions
	- Thông báo hiển thị lỗi ở các bài test với DOM chưa rõ ràng, dẫn tới khó debug
	- Hiện nay, `jest-dom` đã giải quyết vấn đề này, các error message hiển thị cụ thể lỗi
	- Ví dụ
	- Không sử dụng `jest-dom`
	```
	'type error cannot read property type of null'
	```
	
	- Sử dụng `jest-dom`
	```
	expect(received).toHaveAttribute()

	received value must be an HTMLElement or an SVGElement. Received has value: null
	```

	- Cách cài đặt `jest-dom`
	```
	jest-dom.js
	
	import {toHaveAttribute} from '@testing-library/jest-dom'

	expect.extend({toHaveAttribute})

	test('renders a number input with a label "Favorite Number"', () => {
	  const div = document.createElement('div')
	  ReactDOM.render(<FavoriteNumber />, div)
	  expect(div.querySelector('input')).toHaveAttribute('type', 'number')
	  expect(div.querySelector('label').textContent).toHaveAttribute('Favorite Number'))
	})
	```
	- Hoặc
	```
	import * as jestDOM from '@testing-library/jest-dom'

	expect.extend(jestDOM)
	```
	- Thậm chí `jest-dom` đã được cấu hình tự động import vào mỗi bài test, không cần khai báo.
	
4. Use DOM Testing Library to Write More Maintainable React Tests
	- DOM Testing Library cung cấp nhiều tùy biến cho function render để hữu ích trong việc tìm kiếm các element trong DOM như cách người dùng sẽ làm
	- Một số API:
		- queries
		- getQueriesForElement
	- Ví dụ:
	```
	dom-testing-library.js
	
	import {queries} from '@testing-library/dom'
	
	test('renders a number input with a label "Favorite Number"', () => {
	  const div = document.createElement('div')
	  ReactDOM.render(<FavoriteNumber />, div)
	  const input = queries.getByLabelText(div, 'Favorite Number')
	  expect(input).toHaveAttribute('type', 'number')
	})
	```
	